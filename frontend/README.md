# General info

This project is created with create-react-app with Typescript using Flask as a backend service.

## Prettier and ESLint

Use this [config](https://medium.com/technical-credit/using-prettier-with-vs-code-and-create-react-app-67c2449b9d08) to get up and running with Prettier and ESLint.
Everything is written in TypeScript.

## Styled components

We use [styled components](https://www.styled-components.com/docs/basics) for styling. It's recommended to install a plugin in your IDE to support this.

## Design

We use Figma for design. The board is found [here](https://www.figma.com/file/6gFvRPqSiTfha2PGAOB6aA/NTNU-beredskap?node-id=6%3A393)

# Setup

We use [Node v12.](https://nodejs.org/en/download/) with [Yarn](https://yarnpkg.com/lang/en/docs/install/#mac-stable)

## Run project

`$ yarn install`

`$ yarn start`
