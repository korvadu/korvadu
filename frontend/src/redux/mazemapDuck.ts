import { ReduxAction } from './../shared/types/types';
import axios from 'axios';
import { Dispatch } from 'redux';
// Creating a new instance removes default config (access token) that we don't want to send to external APIs
const axiosInstance = axios.create();

// Actions
const SEARCH_POI = 'mazemap/SEARCH_POI';
const SEARCH_POI_FULFILLED = 'mazemap/SEARCH_POI_FULFILLED';
const SEARCH_POI_PENDING = 'mazemap/SEARCH_POI_PENDING';
const SEARCH_POI_REJECTED = 'mazemap/SEARCH_POI_REJECTED';

const LOAD_CAMPUSES = 'mazemap/LOAD_CAMPUSES';
const LOAD_CAMPUSES_FULFILLED = 'mazemap/LOAD_CAMPUSES_FULFILLED';
const LOAD_CAMPUSES_PENDING = 'mazemap/LOAD_CAMPUSES_PENDING';
const LOAD_CAMPUSES_REJECTED = 'mazemap/LOAD_CAMPUSES_REJECTED';

const initialState = {
  loading: false,
  searchResults: [],
};
// Reducer
const reducer = (state = initialState, action: ReduxAction) => {
  switch (action.type) {
    case LOAD_CAMPUSES_PENDING:
      return { ...state, loading: true };
    case LOAD_CAMPUSES_FULFILLED:
      return { ...state, campuses: action.payload.data, loading: false };
    case LOAD_CAMPUSES_REJECTED:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case SEARCH_POI_PENDING:
      return { ...state, searchResults: action.payload, loading: true };
    case SEARCH_POI_FULFILLED:
      return { ...state, searchResults: action.payload, loading: false };
    case SEARCH_POI_REJECTED:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

// Functions
export const searchPoi = (poi: string, campusId: number) => {
  return async (dispatch: Dispatch) => {
    dispatch({
      type: SEARCH_POI,
      payload: axiosInstance.get(
        `https://api.mazemap.com/search/equery/?q=${poi}&rows=10&start=0&withpois=true&withbuilding=false&withtype=false&withcampus=false&campusid=${campusId}&campuscollectiontag=default`
      ),
    });
  };
};

export const getCampuses = (collection: string) => {
  return async (dispatch: Dispatch) => {
    dispatch({
      type: LOAD_CAMPUSES,
      payload: axiosInstance.get(
        `https://api.mazemap.com/api/campuscollection/${collection}/?withcampuses=true`
      ),
    });
  };
};
export default reducer;
