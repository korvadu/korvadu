import { ReduxAction, Registration } from '../shared/types/types';
import axios from 'axios';
import { Dispatch } from 'redux';
import { BACKEND } from '../config';
import { handleHttpResponse } from '../shared/functions/functions';

// Actions
const LOAD_REGISTRATIONS = 'registrations/LOAD_REGISTRATIONS';
const LOAD_REGISTRATIONS_FULFILLED =
  'registrations/LOAD_REGISTRATIONS_FULFILLED';
const LOAD_REGISTRATIONS_PENDING = 'registrations/LOAD_REGISTRATIONS_PENDING';
const LOAD_REGISTRATIONS_REJECTED = 'registrations/LOAD_REGISTRATIONS_REJECTED';

const DELETE_REGISTRATION = 'registrations/DELETE_REGISTRATION';
const DELETE_REGISTRATION_FULFILLED =
  'registrations/DELETE_REGISTRATION_FULFILLED';
const DELETE_REGISTRATION_PENDING = 'registrations/DELETE_REGISTRATION_PENDING';
const DELETE_REGISTRATION_REJECTED =
  'registrations/DELETE_REGISTRATION_REJECTED';

const CREATE_REGISTRATION = 'registrations/CREATE_REGISTRATION';
const CREATE_REGISTRATION_FULFILLED =
  'registrations/CREATE_REGISTRATION_FULFILLED';
const CREATE_REGISTRATION_PENDING = 'registrations/CREATE_REGISTRATION_PENDING';
const CREATE_REGISTRATION_REJECTED =
  'registrations/CREATE_REGISTRATION_REJECTED';

const UPDATE_REGISTRATION = 'registrations/UPDATE_REGISTRATION';
const UPDATE_REGISTRATION_FULFILLED =
  'registrations/UPDATE_REGISTRATION_FULFILLED';
const UPDATE_REGISTRATION_PENDING = 'registrations/UPDATE_REGISTRATION_PENDING';
const UPDATE_REGISTRATION_REJECTED =
  'registrations/UPDATE_REGISTRATION_REJECTED';

const initialState = {
  loading: false,
  registrations: [],
};
// Reducer
const reducer = (state = initialState, action: ReduxAction) => {
  switch (action.type) {
    case LOAD_REGISTRATIONS_PENDING:
      return { ...state, loading: true, error: null };
    case LOAD_REGISTRATIONS_FULFILLED:
      return { ...state, registrations: action.payload.data, loading: false };
    case LOAD_REGISTRATIONS_REJECTED:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case DELETE_REGISTRATION_PENDING:
      return { ...state, loading: true };
    case DELETE_REGISTRATION_REJECTED:
      return { ...state, loading: false, error: action.payload };

    case DELETE_REGISTRATION_FULFILLED:
      return {
        ...state,
        loading: false,
        registrations: state.registrations.filter(
          (item) => item.id !== action.payload.data.id
        ),
      };

    case CREATE_REGISTRATION_PENDING:
      return { ...state, loading: true, error: null };
    case CREATE_REGISTRATION_FULFILLED:
      return {
        ...state,
        registrations: [...state.registrations, action.payload.data],
        loading: false,
      };
    case CREATE_REGISTRATION_REJECTED:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case UPDATE_REGISTRATION_PENDING:
      return { ...state, loading: true, error: null };
    case UPDATE_REGISTRATION_FULFILLED:
      let newRegistrations = [
        ...state.registrations.filter(
          (item) => item.id !== action.payload.data.id
        ),
        action.payload.data,
      ];
      return {
        ...state,
        registrations: newRegistrations.sort(
          (a, b) =>
            new Date(b.starttime).valueOf() - new Date(a.starttime).valueOf()
        ),
        loading: false,
      };
    case UPDATE_REGISTRATION_REJECTED:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

// Functions
export const getRegistrations = (oidc) => {
  return async (dispatch: Dispatch) => {
    dispatch({
      type: LOAD_REGISTRATIONS,
      payload: axios.get(`${BACKEND}/registration/`, {}),
    });
  };
};

export const deleteRegistration = (oidc: any, id: number) => {
  return async (dispatch: Dispatch) => {
    dispatch({
      type: DELETE_REGISTRATION,
      payload: axios.delete(`${BACKEND}/registration/${id}`, {}),
    });
  };
};

export const createRegistration = (oidc: any, registration: Registration) => {
  return async (dispatch: Dispatch) => {
    const response = dispatch({
      type: CREATE_REGISTRATION,
      payload: axios.post(`${BACKEND}/registration/`, registration, {}),
    });
    handleHttpResponse(response, 'Oppholdet ditt er registrert! Takk 😊');
  };
};

export const updateRegistration = (
  oidc: any,
  id: number,
  registration: Registration
) => {
  return async (dispatch: Dispatch) => {
    const response = dispatch({
      type: UPDATE_REGISTRATION,
      payload: axios.put(`${BACKEND}/registration/${id}`, registration, {}),
    });
    handleHttpResponse(response, 'Oppholdet ditt er oppdatert! Takk 😊');
  };
};

export default reducer;
