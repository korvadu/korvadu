import { ReduxAction } from '../shared/types/types';
import axios from 'axios';
import { Dispatch } from 'redux';
import { BACKEND } from '../config';
import * as Sentry from '@sentry/react';
import { handleHttpResponse } from '../shared/functions/functions';

// Actions
const CLEAR_USER = 'USER/CLEAR_USER';

const LOAD_USER = 'USER/LOAD_USER';
const LOAD_USER_FULFILLED = 'USER/LOAD_USER_FULFILLED';
const LOAD_USER_PENDING = 'USER/LOAD_USER_PENDING';
const LOAD_USER_REJECTED = 'USER/LOAD_USER_REJECTED';

const UPDATE_USER = 'USER/UPDATE_USER';
const UPDATE_USER_FULFILLED = 'USER/UPDATE_USER_FULFILLED';
const UPDATE_USER_PENDING = 'USER/UPDATE_USER_PENDING';
const UPDATE_USER_REJECTED = 'USER/UPDATE_USER_REJECTED';

const DELETE_USER = 'USER/DELETE_USER';
const DELETE_USER_FULFILLED = 'USER/DELETE_USER_FULFILLED';
const DELETE_USER_PENDING = 'USER/DELETE_USER_PENDING';
const DELETE_USER_REJECTED = 'USER/DELETE_USER_REJECTED';

const initialState = {
  loading: false,
  user: {
    email: '',
    fullname: '',
  },
};
// Reducer
const reducer = (state = initialState, action: ReduxAction) => {
  switch (action.type) {
    case CLEAR_USER:
      return { ...state, user: null, loading: true };
    case LOAD_USER_PENDING:
      return { ...state, loading: true };
    case LOAD_USER_FULFILLED:
      return { ...state, user: action.payload.data, loading: false };
    case LOAD_USER_REJECTED:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case UPDATE_USER_PENDING:
      return { ...state, loading: true };
    case UPDATE_USER_FULFILLED:
      return {
        ...state,
        user: { ...state.user, ...action.payload.data },
        loading: false,
      };
    case UPDATE_USER_REJECTED:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case DELETE_USER_PENDING:
      return { ...state, loading: true };
    case DELETE_USER_FULFILLED:
      Sentry.captureMessage(`Deleted user`);
      return {
        ...state,
        user: { ...state.user, ...action.payload.data },
        loading: false,
      };
    case DELETE_USER_REJECTED:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

// Functions

export const loadUser = (oidc) => {
  return async (dispatch: Dispatch) => {
    dispatch({
      type: LOAD_USER,
      payload: axios.get(`${BACKEND}/user/`, {}),
    });
  };
};

export const updateUser = (oidc: any, userInfo: { phonenumber: string }) => {
  return async (dispatch: Dispatch) => {
    const response = dispatch({
      type: UPDATE_USER,
      payload: axios.put(`${BACKEND}/user/`, userInfo, {}),
    });
    handleHttpResponse(response, 'Telefonnummeret ditt er oppdatert! Takk 😊');
  };
};

export const clearUser = () => {
  return async (dispatch: Dispatch) => {
    dispatch({
      type: CLEAR_USER,
      payload: {},
    });
  };
};

export const deleteUser = (oidc: any) => {
  return async (dispatch: Dispatch) => {
    const response = dispatch({
      type: DELETE_USER,
      payload: axios.delete(`${BACKEND}/user/`, {}),
    });
    handleHttpResponse(response, 'Brukeren din har blitt slettet.');
  };
};

export default reducer;
