import React, { useEffect, useState } from 'react';
import {
  getRegistrations,
  deleteRegistration,
} from '../../redux/registrationDuck';
import { Registration, RootState } from '../../shared/types/types';
import { useDispatch, useSelector } from 'react-redux';
import Loader from '../../components/loader/Loader';
import Header from '../../components/header/Header';
import RegisterCard from '../../components/registerCard/RegisterCard';
import styled from 'styled-components';
import ConfirmationModal from '../../components/confirmationModal/ConfirmationModal';
import DeleteModal from '../../components/confirmationModal/DeleteModal';
import { format, isSameDay } from 'date-fns';
import { useHistory } from 'react-router-dom';
import { nb } from 'date-fns/locale';
import Hamburger from '../../components/hamburger/Hamburger';
import EmptyView from '../../components/placeholderView/EmptyView';

interface Props {}

const DateHeader = styled.h1`
  font-size: 20px;
  padding-top: 2em;
  margin: 0 1em;
`;

const Wrapper = styled.div`
  min-height: 100vh;
`;

const RegistrationWrapper = styled.div`
  max-width: 1000px;
  display: block;
  margin: auto;

  h1:first-child {
    padding-top: 0;
  }
`;

const MyRegistrations = (props: Props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const registration = useSelector((state: RootState) => state.registration);
  const oidc = useSelector((state: RootState) => state.oidc);

  const [openModal, setOpenModal] = useState(false);
  const [selectedId, setSelectedId] = useState(0);

  useEffect(() => {
    dispatch(getRegistrations(oidc));
  }, [dispatch, oidc]);

  const openDeleteModal = (id: number) => {
    setOpenModal(true);
    setSelectedId(id);
  };

  const goToEdit = (id: number) => {
    setSelectedId(id);
    history.push({
      pathname: `/edit/`,
      state: {
        registration: registration.registrations.find((elem) => elem.id === id),
      },
    });
  };

  const goToReuse = (id: number) => {
    setSelectedId(id);
    history.push({
      pathname: `/reuse/`,
      state: {
        registration: registration.registrations.find((elem) => elem.id === id),
      },
    });
  };

  const handleDelete = () => {
    setOpenModal(false);
    if (selectedId !== null) dispatch(deleteRegistration(oidc, selectedId));
  };

  const registrationCard = (registration: Registration) => {
    const starttime = new Date(registration.starttime);
    const endtime = new Date(registration.endtime);
    return (
      <RegisterCard
        key={registration.id}
        id={registration.id}
        campus={registration.campus}
        building={registration.building}
        room={registration.room}
        start={starttime && format(starttime, 'kk:mm')}
        end={endtime && format(endtime, 'kk:mm')}
        onDelete={openDeleteModal}
        onEdit={goToEdit}
        onReuse={goToReuse}
      />
    );
  };

  let lastDate = null;

  return (
    <Wrapper>
      <ConfirmationModal
        open={openModal}
        closeModal={() => setOpenModal(false)}
        text={'Slett'}
        onActionClick={handleDelete}
      >
        {registration.registrations && (
          <DeleteModal
            registration={registration.registrations.find(
              (elem) => elem.id === selectedId
            )}
          />
        )}
      </ConfirmationModal>
      <Header
        text="Mine registreringer"
        imageSource="images/my-registrations.svg"
      />
      <Hamburger />

      <RegistrationWrapper>
        {registration?.registrations.length === 0 ? (
          <EmptyView
            image="svg-icons/no-registrations.svg"
            text="Du har for øyeblikket ingen registreringer. Fremtidige registreringer vil arkiveres på denne siden."
          ></EmptyView>
        ) : (
          registration.registrations.map(
            (elem: Registration, index: number) => {
              const starttime = new Date(elem.starttime);
              if (lastDate === null || !isSameDay(lastDate, starttime)) {
                lastDate = starttime;
                return (
                  <React.Fragment key={`span-${index}`}>
                    <DateHeader key={`date-${index}`}>
                      {starttime &&
                        format(starttime, 'd. LLLL yyyy', { locale: nb })}
                    </DateHeader>
                    {registrationCard(elem)}
                  </React.Fragment>
                );
              }

              return registrationCard(elem);
            }
          )
        )}
        <Loader loading={registration.loading} />
      </RegistrationWrapper>
    </Wrapper>
  );
};

export default MyRegistrations;
