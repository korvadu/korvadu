import React, { useState, useEffect } from 'react';
import Header from '../../components/header/Header';
import styled from 'styled-components';
import roundToNearestMinutes from 'date-fns/roundToNearestMinutes';
import {
  createRegistration,
  updateRegistration,
} from '../../redux/registrationDuck';
import { updateUser } from '../../redux/userDuck';
import { useDispatch, useSelector } from 'react-redux';

import RegistrationForm from '../../components/registrationForm/RegistrationForm';
import {
  PageType,
  Registration,
  RoomInfo,
  RootState,
} from '../../shared/types/types';
import { useLocation, useHistory } from 'react-router-dom';
import Hamburger from '../../components/hamburger/Hamburger';
import ConfimationModal from '../../components/confirmationModal/ConfirmationModal';
import PhoneModal from '../../components/confirmationModal/PhoneModal';
import errorToast from '../../components/toast/errorToast';
import { setHours, setMinutes, getHours, getMinutes } from 'date-fns';
import 'react-phone-number-input/style.css';
import { isValidPhoneNumber } from 'react-phone-number-input';

interface RegistrationProps {
  pageType: PageType;
}
export interface Location {
  registration: Registration;
}

const RegistrationView = (props: RegistrationProps) => {
  const location = useLocation<Location>();
  const dispatch = useDispatch();
  const history = useHistory();
  const oidc = useSelector((state: RootState) => state.oidc);
  const user = useSelector((state: RootState) => state.user);

  const [selectedRoom, setSelectedRoom] = useState<RoomInfo>();
  const [date, setDate] = useState(new Date());
  const [startTime, setStartTime] = useState(
    roundToNearestMinutes(new Date(), { nearestTo: 15 })
  );
  const [endTime, setEndTime] = useState(
    roundToNearestMinutes(new Date(), { nearestTo: 15 })
  );
  const [openPhoneModal, setOpenPhoneModal] = useState(false);
  const [phonenumber, setPhonenumber] = useState('');

  const clearInputs = () => {
    setSelectedRoom(null);
    setStartTime(roundToNearestMinutes(new Date(), { nearestTo: 5 }));
    setEndTime(roundToNearestMinutes(new Date(), { nearestTo: 5 }));
  };

  const registration = location.state?.registration;

  useEffect(() => {
    const setSelected = () => {
      setSelectedRoom({
        campus: registration.campus,
        room: registration.room,
        building: registration.building,
        poiId: registration.poiId,
      });
    };
    if (props.pageType === 'edit') {
      setSelected();
      setStartTime(new Date(registration.starttime));
      setEndTime(new Date(registration.endtime));
    } else if (props.pageType === 'reuse') {
      setSelected();
      const starttime = new Date(registration.starttime);
      const endtime = new Date(registration.endtime);
      setStartTime(changeToday(starttime));
      setEndTime(changeToday(endtime));
    }
    if (user.user && user.user.phonenumber === null && !user.loading) {
      setOpenPhoneModal(true);
    }
  }, [registration, props.pageType, user, dispatch]);

  const changeToday = (date: Date): Date => {
    return setHours(setMinutes(new Date(), getMinutes(date)), getHours(date));
  };

  const onSubmit = (event: Event) => {
    event.preventDefault();
    // TODO: Need to add userid and building
    const newRegistration = {
      room: selectedRoom.room,
      building: selectedRoom.building,
      campus: selectedRoom.campus,
      poiId: selectedRoom.poiId,
      starttime: startTime,
      endtime: endTime,
    };

    if (props.pageType === 'edit') {
      dispatch(updateRegistration(oidc, registration.id, newRegistration));
      history.push('/my-registrations');
    } else {
      dispatch(createRegistration(oidc, newRegistration));
    }

    clearInputs();
  };

  const changePhonenumber = () => {
    const userInfo = {
      phonenumber: phonenumber,
    };

    if (isValidPhoneNumber(phonenumber)) {
      dispatch(updateUser(oidc, userInfo));
      setOpenPhoneModal(false);
    } else {
      errorToast('Ugyldig telefonnummer');
    }
  };

  const handleChangeRoom = (selectedOption: RoomInfo) => {
    setSelectedRoom(selectedOption);
  };
  const buttonDisabled =
    selectedRoom && selectedRoom.room !== '' && startTime && endTime
      ? false
      : true;
  const isEdit = props.pageType === 'edit';
  return (
    <RegistrationContainer>
      <ConfimationModal
        text="Registrer telefonnummer"
        open={openPhoneModal}
        closeModal={() => setOpenPhoneModal(false)}
        onActionClick={changePhonenumber}
        notExit={true}
      >
        <PhoneModal onChange={setPhonenumber} phonenumber={phonenumber} />
      </ConfimationModal>
      <Hamburger />
      <Header
        text={isEdit ? 'Endre opphold' : 'Registrer opphold'}
        imageSource="/images/registration.svg"
      />
      <RegistrationForm
        onSubmit={onSubmit}
        selectedRoom={selectedRoom && selectedRoom}
        handleChangeRoom={handleChangeRoom}
        date={date}
        setDate={setDate}
        startTime={startTime}
        setStartTime={setStartTime}
        endTime={endTime}
        setEndTime={setEndTime}
        buttonDisabled={buttonDisabled}
        buttonText={isEdit ? 'Endre opphold' : 'Registrer opphold'}
      />
    </RegistrationContainer>
  );
};

const RegistrationContainer = styled.div`
  min-height: 100vh;
`;

export default RegistrationView;
