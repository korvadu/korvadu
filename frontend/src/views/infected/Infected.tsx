import React from 'react';
import Header from '../../components/header/Header';
import Title from '../../components/title/Title';
import Text from '../../components/text/Text';
import ContentContainer from '../../components/container/ContentContainer';
import styled from 'styled-components';
import theme from '../../shared/theme/theme';
import Hamburger from '../../components/hamburger/Hamburger';
interface Props {}

const Infected = (props: Props) => {
  return (
    <div>
      <Header
        text="DERSOM DU ER SMITTET"
        imageSource="images/infected.svg"
      ></Header>
      <Hamburger />
      <ContentContainer>
        <Title>Er du blitt smittet?</Title>
        <TextContainer>
          <Text>
            Dersom du har testet positivt på Covid 19 etter å ha oppholdt deg på
            NTNU sine campus, ta kontakt på dette nummeret:
          </Text>
        </TextContainer>
        <PhoneNumberContainer>
          <PhoneNumberIcon src="svg-icons/phone.svg"></PhoneNumberIcon>
          <Text>+4799123456</Text>
        </PhoneNumberContainer>
        <Text>Mer info finner du på NTNU sine sider:</Text>
        <Link href="https://www.ntnu.no/korona">
          <Text>https://www.ntnu.no/korona</Text>
        </Link>
      </ContentContainer>
    </div>
  );
};

const PhoneNumberContainer = styled.div`
  display: flex;
  align-self: center;
  padding: 30px;
`;
const PhoneNumberIcon = styled.img`
  width: 30px;
  height: 30px;
`;

const Link = styled.a`
  color: ${theme.colors.gray};
  &:visited {
    color: ${theme.colors.gray};
  }
`;

const TextContainer = styled.div`
  margin-top: 10px;
`;

export default Infected;
