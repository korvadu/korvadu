import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import Header from '../../components/header/Header';
import Title from './../../components/title/Title';
import ContentContainer from '../../components/container/ContentContainer';
import Text from '../../components/text/Text';
import Button from '../../components/button/Button';
import { useSelector, useDispatch } from 'react-redux';
import { updateUser, deleteUser } from '../../redux/userDuck';
import errorToast from '../../components/toast/errorToast';
import Hamburger from '../../components/hamburger/Hamburger';
import ConfimationModal from '../../components/confirmationModal/ConfirmationModal';
import DeleteUserModal from '../../components/confirmationModal/DeleteUserModal';
import { useHistory } from 'react-router-dom';
import { RootState } from '../../shared/types/types';
import { logout } from '../../shared/functions/functions';
import { isValidPhoneNumber } from 'react-phone-number-input';
import PhoneInput from 'react-phone-number-input';

interface Props {}

const Spacer = styled.div`
  margin: 1em;
`;

const ButtonContainer = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 2rem;
`;

const MyProfile = (props: Props) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const user = useSelector((state: RootState) => state.user);
  const oidc = useSelector((state: RootState) => state.oidc);
  const [phonenumber, setPhonenumber] = useState<string>();
  const [openModal, setOpenModal] = useState<boolean>(false);

  useEffect(() => {
    if (user.user.phonenumber) {
      setPhonenumber(user.user.phonenumber);
    }
  }, [user]);

  const changeTelephoneNumber = () => {
    const userInfo = {
      phonenumber: phonenumber,
    };

    if (isValidPhoneNumber(phonenumber)) {
      dispatch(updateUser(oidc, userInfo));
    } else {
      errorToast('Ugyldig telefonnummer');
    }
  };

  const handleLogout = () => {
    logout(dispatch);
  };

  const deleteMyProfile = () => {
    dispatch(deleteUser(oidc));
    setOpenModal(false);

    handleLogout();

    history.push('/');
  };

  return (
    <>
      <ConfimationModal
        open={openModal}
        text="Slett bruker"
        onActionClick={deleteMyProfile}
        closeModal={() => setOpenModal(false)}
      >
        <DeleteUserModal />
      </ConfimationModal>
      <Header text="PROFIL" imageSource="images/my-profile.svg"></Header>
      <Hamburger />

      <ContentContainer>
        <Title>{user.user.fullname}</Title>
        <PhoneInput
          placeholder="Telefonnummeret ditt"
          value={phonenumber}
          onChange={setPhonenumber}
          defaultCountry="NO"
          className="phoneInput "
        />
        <ButtonContainer>
          <Button value="Endre" width="284px" onClick={changeTelephoneNumber} />
        </ButtonContainer>

        <Title>HVORFOR TRENGER VI NAVN OG NUMMER?</Title>

        <Text>Tekst om hvorfor navn og telefonnummer oppgis</Text>

        <Spacer />
        <Title>SLETT MINE DATA</Title>

        <Text>
          Tekst om hva sletting av data innebærer og hvor enkelt det er
        </Text>

        <Spacer />

        <ButtonContainer>
          <Button
            value="Slett mine data"
            width="284px"
            onClick={() => setOpenModal(true)}
          />
        </ButtonContainer>
      </ContentContainer>
    </>
  );
};

export default MyProfile;
