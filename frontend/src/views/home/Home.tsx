import React from 'react';
import Header from '../../components/header/Header';
import Hamburger from '../../components/hamburger/Hamburger';
import Title from '../../components/title/Title';
import Text from '../../components/text/Text';
import ContentContainer from '../../components/container/ContentContainer';
import styled from 'styled-components';

interface Props {}

const Spacer = styled.div`
  margin: 1em;
`;

const Home = (props: Props) => {
  return (
    <div>
      <Hamburger />
      <Header text="Om Korvadu?" imageSource="images/information.svg"></Header>

      <ContentContainer>
        <Title>Hva er Korvadu?</Title>
        <Text>
          Korvadu? er en nettside med formål om å raskt kunne informere elever
          og ansatte ved potensiell smitte. Vårt mål er å skape en tryggere
          hverdag for dere på campus. Du kan hjelpe oss med å nå dette målet ved
          å registrere hvilke rom du har benyttet deg av. Takk for hjelpen.
        </Text>

        <Spacer />

        <Title>Hvem har tilgang til dataen min?</Title>
        <Text>
          Dataen din blir lagret hos NTNU. Vi vil kun benytte oss av dataen din
          om du har vært på samme sted med en annen som er bekreftet smittet. Du
          kan når som helst velge å fjerne dataen du har oppgitt ved å benytte
          deg av "Slett mine data" knappen under "Min profil" siden.
        </Text>
      </ContentContainer>
    </div>
  );
};

export default Home;
