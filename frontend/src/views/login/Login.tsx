import userManager from '../../auth/userManager';
import { useHistory } from 'react-router-dom';
import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';
import theme from '../../shared/theme/theme';
import Button from '../../components/button/Button';
import { RootState } from '../../shared/types/types';
const Login: React.FunctionComponent<{}> = (props) => {
  const doLogin = (event: React.MouseEvent) => {
    event.preventDefault();
    userManager.signinRedirect();
  };
  const onClickAction = (event: React.MouseEvent) => doLogin(event);
  const oidc = useSelector((state: RootState) => state.oidc);
  const history = useHistory();
  useEffect(() => {
    if (oidc.user) {
      history.push('/');
    }
  }, [oidc.user, history]);
  return (
    <LoginContainer>
      <LoginBackground src="/background-vectors/login-background.svg"></LoginBackground>
      <ContentContainer>
        <ContentInnerContainer>
          <HeaderContainer>
            <KorvaduLogo src="/logos/logo-korvadu.svg"></KorvaduLogo>
            <KorvaduText>KorVaDu?</KorvaduText>
          </HeaderContainer>
          <ExplanationText>
            Hjelp oss med å begrense smittespredningen ved NTNU. Registrerer
            hvilke rom du har oppholdt deg på slik at vi kan varsle. Vi sporer
            deg ikke, dette er manuelt og trygt, null stress å logge inn.
          </ExplanationText>
          <ButtonContainer>
            <Button
              value="Logg inn"
              backgroundColor="white"
              color="black"
              onClick={onClickAction}
              fontWeight="bold"
              width="200px"
            />
          </ButtonContainer>
        </ContentInnerContainer>
      </ContentContainer>
      <Hand src="/images/hand.svg"></Hand>
      <NtnuLogo src="/logos/logo-ntnu.png"></NtnuLogo>
    </LoginContainer>
  );
};

const ExplanationText = styled.div`
  font-size: 25px;
  @media (max-width: 1000px) {
    font-size: 20px;
  }
  @media (max-width: 400px) {
    font-size: 17px;
  }
  @media (max-width: 350px) {
    font-size: 15px;
  }
  color: ${theme.colors.white};
  margin: 50px 0px 50px 0px;
`;
const HeaderContainer = styled.div`
  display: flex;
`;
const ContentInnerContainer = styled.div`
  width: 45%;
  @media (max-width: 1000px) {
    width: 80%;
    margin-top: 15vh;
  }
`;
const KorvaduText = styled.p`
  color: ${theme.colors.white};
  font-size: 50px;
  @media (max-width: 1000px) {
    font-size: 30px;
  }
  font-weight: bold;
  margin-top: 10px;
`;
const KorvaduLogo = styled.img`
  margin-right: 20px;
  @media (max-width: 1000px) {
    height: 50px;
  }
`;

const ButtonContainer = styled.div`
  z-index: 2;
  position: relative;
`;

const ContentContainer = styled.div`
  position: fixed;

  height: 60vh;
  top: 20vh;

  width: 70vw;
  right: -15vw;

  @media (max-width: 1300px) {
    right: -10vw;
  }
  @media (max-width: 1000px) {
    width: 100%;
    position: relative;
    right: 0;
    top: 0;
    height: auto;
  }

  display: flex;
  justify-content: center;
`;
const NtnuLogo = styled.img`
  position: fixed;

  bottom: 15px;
  right: 15px;
  height: 20px;
`;
const Hand = styled.img`
  position: fixed;
  z-index: 1;

  bottom: 0px;
  left: 0px;
  height: 40vh;
  @media (max-width: 500px) {
    height: 27vh;
  }
  @media (max-width: 400px) {
    height: 24vh;
  }
`;

const LoginContainer = styled.div`
  display: flex;
  overflow-x: hidden;
  max-width: 100%;
`;
const LoginBackground = styled.img`
  position: fixed;
  overflow-x: hidden;
  height: 100vh;
  top: -10vh;

  width: 80vw;
  right: -20vw;

  @media (max-width: 2000px) {
    height: 100vh;
    top: -10vh;

    width: 100vw;
    right: -20vw;
  }
  @media (max-width: 1300px) {
    height: 100vh;
    top: -10vh;

    width: 90vw;
    right: -10vw;
  }
  @media (max-width: 1000px) {
    height: 85vh;
    top: -5vh;

    width: 200vw;
    right: -50vw;
  }
`;

export default Login;
