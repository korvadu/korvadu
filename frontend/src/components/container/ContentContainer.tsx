import React from 'react';
import styled from 'styled-components';

interface Props {
  children: React.ReactNode;
}

const OuterContainer = styled.div`
  display: flex;
  justify-content: center;
`;
const InnerContainer = styled.div`
  display: flex;
  width: 60%;
  flex-direction: column;
  align-items: center;
`;

const ContentContainer = ({ children }: Props) => {
  return (
    <OuterContainer>
      <InnerContainer>{children}</InnerContainer>
    </OuterContainer>
  );
};

export default ContentContainer;
