import React from 'react';
import styled from 'styled-components';
import theme from '../../shared/theme/theme';

interface Props {
  value?: string;
  onClick?: any;
  disabled?: boolean;
  width?: string;
  style?: any;
  backgroundColor?: ButtonColor;
  color?: ButtonColor;
  fontWeight?: string;
}

const buttonColors = {
  standard: theme.colors.primary,
  white: theme.colors.white,
  black: theme.colors.black,
};

type ButtonColor = 'standard' | 'white' | 'black';

const Button = (props: Props) => {
  return (
    <SubmitButton
      {...props}
      style={{
        fontWeight: props.fontWeight ? props.fontWeight : 400,
        backgroundColor: props.backgroundColor
          ? buttonColors[props.backgroundColor]
          : buttonColors['standard'],
        color: props.color ? buttonColors[props.color] : buttonColors['white'],
        width: props.width ? props.width : 'auto',
        ...props.style,
      }}
      type="submit"
    ></SubmitButton>
  );
};

const SubmitButton = styled.input`
  padding: 15px;
  border: none;
  cursor: pointer;
  &:disabled {
    background-color: ${theme.colors.disabled} !important;
    cursor: not-allowed;
  }
  border-radius: 4px;

  &:hover:enabled {
    filter: brightness(125%);
  }
`;

export default Button;
