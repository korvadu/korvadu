import React from 'react';
import DatePicker from 'react-datepicker';
import setHours from 'date-fns/setHours';
import setMinutes from 'date-fns/setMinutes';
import styled from 'styled-components';
import theme from '../../../shared/theme/theme';
import InputLabel from '../../inputLabel/InputLabel';
import { isBefore } from 'date-fns';
import { isMobile } from 'react-device-detect';

interface TimeInputProps {
  date: Date;
  setDate: (date: Date) => void;
  startTime: Date;
  setStartTime: (date: Date) => void;
  endTime: Date;
  setEndTime: (date: Date) => void;
}

const TimeInput = ({
  date,
  setDate,
  startTime,
  setStartTime,
  endTime,
  setEndTime,
}: TimeInputProps) => {
  const handleFocus = (event: React.FocusEvent<HTMLInputElement>) => {
    if (isMobile) {
      event.target.readOnly = true;
    } else {
      event.target.readOnly = false;
    }
  };

  return (
    <>
      <TimeInputOuterContainer>
        <TimeInputInnerContainer>
          <InputLabel>Dato</InputLabel>
          <DatePicker
            selected={date}
            onChange={(date) => {
              if (date != null) {
                setDate(date);
                setStartTime(date);
                setEndTime(date);
              }
            }}
            dateFormat="dd.MM.yy"
            maxDate={new Date()}
            onFocus={handleFocus}
          />
        </TimeInputInnerContainer>
      </TimeInputOuterContainer>

      <TimeInputOuterContainer>
        <TimeInputInnerContainer>
          <InputLabel>Start</InputLabel>
          <DatePicker
            selected={startTime}
            onChange={(date) => {
              if (date != null) {
                setDate(date);
                setStartTime(date);

                //set endtime only if it is before
                if (isBefore(endTime, date)) {
                  setEndTime(date);
                }
              }
            }}
            showTimeSelect
            showTimeSelectOnly
            timeIntervals={15}
            dateFormat="HH:mm"
            timeCaption="Tidspunkt"
            onFocus={handleFocus}
          />
        </TimeInputInnerContainer>
        <TimeInputInnerContainer>
          <InputLabel>Slutt</InputLabel>
          <DatePicker
            selected={endTime}
            onChange={(date) => {
              if (date != null) {
                setEndTime(date);
              }
            }}
            showTimeSelect
            showTimeSelectOnly
            timeIntervals={15}
            minTime={setHours(
              setMinutes(startTime, startTime.getMinutes()),
              startTime.getHours()
            )}
            maxTime={setHours(setMinutes(new Date(), 59), 23)}
            dateFormat="HH:mm"
            timeCaption="Tidspunkt"
            onFocus={handleFocus}
          />
        </TimeInputInnerContainer>
      </TimeInputOuterContainer>
    </>
  );
};

const TimeInputOuterContainer = styled.div`
  display: flex;
  flex-direction: row;
  @media (max-width: 700px) {
    flex-direction: column;
  }
  input {
    width: 98%;
    padding: 1%;
    height: 30px;
    border: 1px solid ${theme.colors.borderGray};
    border-radius: 4px;
    margin-bottom: 20px;
  }
`;

const TimeInputInnerContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  width: 100%;
  &:first-child {
    margin-right: 10px;
  }
`;

export default TimeInput;
