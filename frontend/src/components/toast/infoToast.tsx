import React from 'react';
import Toast from './Toast';
import { toast } from 'react-toastify';

const infoToast = (msg: string) => {
  toast(<Toast message={msg} />, {
    className: 'info-toast',
  });
};

export default infoToast;
