import React from 'react';
import styled from 'styled-components';

const ToastText = styled.p`
  font-weight: 500;
`;

interface ToastProps {
  message: string;
}

const Toast: React.FC<ToastProps> = (props) => {
  return <ToastText>{props.message}</ToastText>;
};

export default Toast;
