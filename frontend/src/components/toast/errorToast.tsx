import React from 'react';
import Toast from './Toast';
import { toast } from 'react-toastify';

const errorToast = (msg: string) => {
  toast.error(<Toast message={'Noe gikk galt! ' + msg} />, {
    className: 'error-toast',
  });
};

export default errorToast;
