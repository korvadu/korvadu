import React from 'react';
import styled from 'styled-components';
import theme from '../../shared/theme/theme';

interface TextProps {
  children: React.ReactNode;
}

const DefaultText = styled.p`
  font-size: 20px;
  color: ${theme.colors.gray};
  margin-bottom: 10px;
  text-align: center;
`;

const Text = ({ children }: TextProps) => {
  return <DefaultText>{children}</DefaultText>;
};

export default Text;
