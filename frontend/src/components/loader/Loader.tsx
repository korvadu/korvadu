import React from 'react';
import { FireworkSpinner } from 'react-spinners-kit';
import theme from '../../shared/theme/theme';
import styled from 'styled-components';

interface LoaderProps {
  loading: boolean;
}

const Loader = (props: LoaderProps) => {
  return (
    <LoaderContainer>
      <FireworkSpinner color={theme.colors.primary} loading={props.loading} />
    </LoaderContainer>
  );
};

const LoaderContainer = styled.div`
  display: flex;
  justify-content: center;
`;

export default Loader;
