import React from 'react';
import Select from 'react-select';
import styled from 'styled-components';

const Selection = (props: any) => {
  const option = props.options.filter(
    (option) => option.label === props.defaultValue
  );
  return (
    <SelectContainer>
      <Select {...props} defaultValue={option}></Select>
    </SelectContainer>
  );
};
const SelectContainer = styled.div`
  margin-bottom: 20px;
`;
export default Selection;
