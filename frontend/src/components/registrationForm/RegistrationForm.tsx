import React from 'react';
import styled from 'styled-components';
import Button from '../button/Button';
import TimeInput from '../inputs/registration/TimeInput';
import { RoomInfo } from '../../shared/types/types';
import MazeMap from '../mazemap/MazeMap';

interface Props {
  onSubmit: (event: any) => void;
  selectedRoom: RoomInfo;
  handleChangeRoom: (room: RoomInfo) => void;
  date: Date;
  setDate: (date: Date) => void;
  startTime: Date;
  setStartTime: (date: Date) => void;
  endTime: Date;
  setEndTime: (date: Date) => void;
  buttonText: string;
  buttonDisabled: boolean;
}
const RegistrationForm = (props: Props) => {
  return (
    <FormContainer>
      <Form>
        <MazeMap
          selectedRoom={props.selectedRoom}
          handleChangeRoom={props.handleChangeRoom}
        ></MazeMap>
        <TimeInput
          date={props.date}
          setDate={props.setDate}
          startTime={props.startTime}
          setStartTime={props.setStartTime}
          endTime={props.endTime}
          setEndTime={props.setEndTime}
        ></TimeInput>
        <Button
          value={props.buttonText}
          disabled={props.buttonDisabled}
          width="auto"
          onClick={props.onSubmit}
        ></Button>
      </Form>
    </FormContainer>
  );
};

const FormContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Form = styled.div`
  display: flex;
  width: 70%;
  max-width: 800px;
  flex-direction: column;
`;
export default RegistrationForm;
