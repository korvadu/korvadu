export const createMap = () => {
  //@ts-ignore
  const map = new Mazemap.Map({
    container: 'map',
    campuses: 'ntnu',
    center: {
      lng: 10.3939051,
      lat: 63.4189104,
    },
    zoom: 13,
    zLevel: 1,
    zLevelControl: false,
    scrollZoom: true,
    doubleClickZoom: true,
    touchZoomRotate: true,
    autoSetRTLTextPlugin: false,
  });

  map.on('load', function () {
    //@ts-ignore
    var customZLevelBar1 = new Mazemap.ZLevelBarControl({
      className: 'custom-zlevel-bar',
      autoUpdate: true,
      maxHeight: 300,
    });
    map.addControl(customZLevelBar1, 'bottom-right');
  });

  return map;
};
