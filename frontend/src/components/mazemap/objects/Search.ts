export const createSearch = (
  containerRef: React.RefObject<HTMLInputElement>,
  inputRef: React.RefObject<HTMLInputElement>,
  suggestionRef: React.RefObject<HTMLInputElement>
) => {
  //@ts-ignore
  const search = new Mazemap.Search.SearchController({
    campuscollectiontag: 'ntnu',
    rows: 30,
    withpois: true,
    withbuilding: false,
    withtype: false,
    withcampus: false,
    resultsFormat: 'geojson',
  });
  //@ts-ignore
  const searchInput = new Mazemap.Search.SearchInput({
    container: document.getElementById('search-input-container'),
    input: document.getElementById('searchInput'),
    suggestions: document.getElementById('suggestions'),
    searchController: search,
  });

  return searchInput;
};
