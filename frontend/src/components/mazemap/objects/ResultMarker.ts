export const createResultMarker = (map: any, searchInput: any) => {
  //@ts-ignore
  const resultMarker = new Mazemap.MazeMarker({
    color: 'rgb(253, 117, 38)',
    innerCircle: true,
    innerCircleColor: '#FFF',
    size: 34,
    innerCircleScale: 0.5,
    zLevel: 1,
  });
  return resultMarker;
};

export const placePoiMarker = (poi: any, map: any, resultMarker: any) => {
  //@ts-ignore
  var lngLat = Mazemap.Util.getPoiLngLat(poi);
  var zLevel = poi.properties.zValue;

  resultMarker.setLngLat(lngLat).setZLevel(poi.properties.zValue).addTo(map);

  map.zLevel = zLevel;

  map.flyTo({
    center: lngLat,
    zoom: 17,
    duration: 2000,
  });
};
