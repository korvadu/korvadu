import React, { useRef, useEffect, useState } from 'react';
import styled from 'styled-components';
import { RoomInfo } from '../../shared/types/types';
import { createMap } from './objects/Map';
import { placePoiMarker, createResultMarker } from './objects/ResultMarker';
import { createSearch } from './objects/Search';
import { campuses } from '../../shared/constants/constants';
import InputLabel from '../inputLabel/InputLabel';
import theme from '../../shared/theme/theme';
interface Props {
  handleChangeRoom: (room: RoomInfo) => void;
  selectedRoom?: RoomInfo;
}

const MazeMap = ({ handleChangeRoom, selectedRoom }: Props) => {
  const [inputValue, setInputValue] = useState('');
  const containerRef = useRef(null);
  const inputRef = useRef(null);
  const suggestionRef = useRef(null);
  const removeEm = (string: string) => {
    return string.replace(/<em>/g, '').replace(/<\/em>/g, '');
  };
  const getData = (poiData: any) => {
    const poiId = poiData.poiId;
    const building =
      poiData.dispBldNames &&
      poiData.dispBldNames[0] &&
      removeEm(poiData.dispBldNames[0]);
    const room =
      poiData.dispPoiNames &&
      poiData.dispPoiNames[0] &&
      removeEm(poiData.dispPoiNames[0]);

    const campus = campuses.find(
      (campus) => campus.campusId === poiData.campusId
    );
    return { poiId, building, room, campus: campus.name };
  };

  useEffect(() => {
    setInputValue(selectedRoom && selectedRoom.room);
  }, [selectedRoom]);

  useEffect(() => {
    const map = createMap();
    const searchInput = createSearch(containerRef, inputRef, suggestionRef);
    const resultMarker = createResultMarker(map, searchInput);
    searchInput.on('itemclick', (e) => {
      var poiFeature = e.item;
      placePoiMarker(poiFeature, map, resultMarker);
      const data = getData(poiFeature.properties);
      handleChangeRoom(data);
    });
    map.getCanvas().addEventListener('focus', function () {
      searchInput.hideSuggestions();
    });
  }, []);

  const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (selectedRoom) {
      handleChangeRoom(null);
    }
    setInputValue(event.target.value);
  };

  return (
    <>
      <InputContainer id="search-input-container" ref={containerRef}>
        <InputLabel>Rom</InputLabel>
        <Input
          tabIndex={1}
          id="searchInput"
          className="search-input"
          autoComplete="off"
          type="text"
          name="search"
          placeholder="Søk rom..."
          ref={inputRef}
          value={inputValue}
          onChange={onChange}
        />
        <Suggestions
          ref={suggestionRef}
          id="suggestions"
          className="search-suggestions default"
        ></Suggestions>
        <MazemapMap id="map" tabIndex={1}></MazemapMap>
      </InputContainer>
    </>
  );
};

const MazemapMap = styled.div`
  width: 100%;
  height: 200px;
  @media (max-width: 600px) {
    height: 120px;
  }
  margin-bottom: 20px;
`;
const InputContainer = styled.div`
  position: relative;
`;
const Suggestions = styled.div`
  position: absolute;
  width: 100%;
  z-index: 10;
`;
const Input = styled.input`
  box-shadow: none;
  border: 1px solid ${theme.colors.borderGray};
  margin-bottom: 10px;
`;
export default MazeMap;
