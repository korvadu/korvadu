import React, { useState, useRef, useEffect } from 'react';

import styled from 'styled-components';

interface Props {
  onDelete: () => void;
  onEdit: () => void;
  onReuse: () => void;
}

const Divider = styled.div`
  border-top: 1px solid #b9b7b7;
  width: 80%;
  display: block;
  margin: auto;
`;

const DropdownContainer = styled.div`
  position: relative;
`;

const DropBox = styled.div`
  z-index: 2;
  position: absolute;
  top: 100%;
  right: 0;
  background-color: #ecf2ff;
  border-radius: 15%;
`;

const DropBoxItem = styled.div`
  padding: 10px 20px;
  cursor: pointer;
`;

const DotContainer = styled.div`
  display: flex;
  justify-content: space-around;
  width: 35px;
  height: 35px;
  cursor: pointer;
  flex-direction: column;
  border-radius: 50%;
  align-items: center;
`;

const Dot = styled.div`
  width: 7px;
  height: 7px;
  border-radius: 50%;
  background-color: #b9b7b7;
`;

const Dropdown = ({ onEdit, onDelete, onReuse }: Props) => {
  const node = useRef<HTMLInputElement | null>(null);

  const [open, setOpen] = useState(false);

  const handleClickOutside = (e) => {
    if (node.current.contains(e.target)) {
      // inside click
      return;
    }

    // outside click
    setOpen(false);
  };

  const handleOnClick = () => {
    setOpen(!open);
  };

  useEffect(() => {
    if (open) {
      document.addEventListener('mousedown', handleClickOutside);
    } else {
      document.removeEventListener('mousedown', handleClickOutside);
    }

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [open]);

  const onClickElement = (actionFunction: () => void) => {
    setOpen(false);
    actionFunction();
  };

  const dropboxContent = (
    <>
      <DropBoxItem onClick={() => onClickElement(onEdit)}>Rediger</DropBoxItem>
      <Divider />
      <DropBoxItem onClick={() => onClickElement(onReuse)}>
        Bruk til ny registrering
      </DropBoxItem>
      <Divider />
      <DropBoxItem onClick={() => onClickElement(onDelete)}>Slett</DropBoxItem>
    </>
  );

  return (
    <DropdownContainer ref={node}>
      <DotContainer onClick={handleOnClick}>
        <Dot />
        <Dot />
        <Dot />
      </DotContainer>

      {open && <DropBox>{dropboxContent}</DropBox>}
    </DropdownContainer>
  );
};

export default Dropdown;
