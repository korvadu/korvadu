import React from 'react';
import styled from 'styled-components';
import theme from '../../shared/theme/theme';

interface InputLabelProps {
  children: React.ReactNode;
}

const TitleContainer = styled.div`
  position: relative;
  display: block;
  margin-bottom: 3px;
`;

const TitleText = styled.div`
  text-align: left;
  color: ${theme.colors.primary};
  font-size: 15px;
  font-weight: bold;
  text-transform: uppercase;
`;

const InputLabel = ({ children }: InputLabelProps) => {
  return (
    <TitleContainer>
      <TitleText>{children}</TitleText>
    </TitleContainer>
  );
};

export default InputLabel;
