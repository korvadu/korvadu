import React from 'react';
import styled from 'styled-components';
import theme from '../../shared/theme/theme';

interface TitleProps {
  children: React.ReactNode;
}

const TitleContainer = styled.div`
  position: relative;
  max-width: 85vw;
  display: block;
  margin: auto;
`;

const TitleText = styled.div`
  text-align: center;
  color: ${theme.colors.primary};
  font-family: ${theme.font.fontFamily};
  font-size: 20px;
  font-weight: bold;
  text-transform: uppercase;
`;

const Title = ({ children }: TitleProps) => {
  return (
    <TitleContainer>
      <TitleText>{children}</TitleText>
    </TitleContainer>
  );
};

export default Title;
