import React from 'react';
import Title from '../title/Title';
import { Registration } from '../../shared/types/types';
import styled from 'styled-components';
import { format } from 'date-fns';
import { nb } from 'date-fns/locale';

interface Props {
  registration?: Registration;
}

const DataDiv = styled.div`
  margin-top: 1em;
  margin-bottom: 1em;
  border-radius: 15px;
  padding: 0.5em;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

const DataItem = styled.div`
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  padding: 0.2em;
`;

const DeleteModal = (props: Props) => {
  const starttime = new Date(props.registration.starttime);
  const endtime = new Date(props.registration.endtime);
  return (
    <>
      <Title>Vil du slette registreringen?</Title>
      <DataDiv>
        <DataItem>
          {starttime && format(starttime, 'd. LLLL yyyy', { locale: nb })}
        </DataItem>
        <DataItem>{props.registration.campus}</DataItem>
        <DataItem>{props.registration.building}</DataItem>
        <DataItem>{props.registration.room}</DataItem>
        <DataItem>
          {starttime && format(starttime, 'k:m')} -{' '}
          {endtime && format(endtime, 'k:m')}
        </DataItem>
      </DataDiv>
    </>
  );
};

export default DeleteModal;
