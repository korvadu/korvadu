import React from 'react';
import styled from 'styled-components';
import Title from '../title/Title';
import PhoneInput from 'react-phone-number-input';

interface Props {
  phonenumber: string;
  onChange: (phonenumber: string) => void;
}

const Container = styled.div`
  max-width: 1000px;
  display: flex;
  margin: auto;
  align-items: center;
  flex-direction: column;
`;
const InfoText = styled.div`
  font-style: normal;
  font-weight: 300;
  font-size: 20px;
  line-height: 140%;
  padding-top: 2em;
  text-align: center;
`;

const PhoneModal = (props: Props) => {
  const handlePhoneChange = (value: string) => {
    props.onChange(value);
  };

  return (
    <Container>
      <Title>Registrer telefonnummer</Title>
      <InfoText>
        For at NTNU raskt skal kunne kontakte personer ved eventuelle
        smittetilfeller må telefonnummer oppgis for å gå videre. Du kan senere
        endre nummeret ditt under "Min profil"
      </InfoText>

      <PhoneInput
        placeholder="Telefonnummeret ditt"
        value={props.phonenumber}
        onChange={handlePhoneChange}
        defaultCountry="NO"
        className="phoneInput"
      />
    </Container>
  );
};

export default PhoneModal;
