import React from 'react';
import styled from 'styled-components';

import Title from '../title/Title';
import theme from '../../shared/theme/theme';

interface Props {}

const Container = styled.div`
  max-width: 1000px;
  display: block;
  margin: auto;
  margin-bottom: 2em;
`;

const InformationText = styled.div`
  font-family: ${theme.font.fontFamily};
  font-style: normal;
  font-weight: 300;
  font-size: 20px;
  line-height: 140%;
  padding-top: 2em;
  display: block;
  margin: auto;
  text-align: center;
`;

const DeleteUserModal = (props: Props) => {
  return (
    <Container>
      <Title>Vil du slette brukeren din?</Title>
      <InformationText>
        Ved å slette dataen din vil den forsvinne for alltid. Det er ikke mulig
        å gjenopprette dataen etter at den har blitt slettet. Dermed har vi ikke
        mulighet til å kontakte deg om du har vært i nærkontakt med en som er
        smittet.
      </InformationText>
    </Container>
  );
};

export default DeleteUserModal;
