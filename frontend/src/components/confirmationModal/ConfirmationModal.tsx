import React from 'react';
import Modal from 'react-modal';
import Button from '../button/Button';
import styled from 'styled-components';
import theme from '../../shared/theme/theme';

interface Props {
  open: boolean;
  closeModal?: () => void;
  children?: React.ReactNode;
  text: string;
  onActionClick: () => void;
  notExit?: boolean;
}
const CloseContainer = styled.div`
  display: flex;
  justify-content: right;
`;
const Close = styled.div`
  display: inline-block;
  font-size: 20px;
  font-weight: bold;
  margin-left: auto;
  cursor: pointer;
`;

const ButtonOuterContainer = styled.div`
  display: flex;
  justify-content: center;
`;

const ButtonInnerContainer = styled.div`
  display: flex;
  max-width: 285px;
  width: 100%;
  margin: 10px;
`;

const customStyles = {
  overlay: {
    backgroundColor: 'rgba(0, 0, 0, 0.75)',
    zIndex: 100,
  },
};

Modal.setAppElement('#root');

const ConfirmationModal = (props: Props) => {
  return (
    <Modal
      isOpen={props.open}
      style={customStyles}
      onRequestClose={props.closeModal}
    >
      {props.notExit ? (
        ''
      ) : (
        <CloseContainer>
          <Close onClick={props.closeModal}>
            <Cross src="/icons/cross.svg"></Cross>
          </Close>
        </CloseContainer>
      )}

      {props.children}
      <ButtonOuterContainer>
        <ButtonInnerContainer>
          <Button
            width="100%"
            value={props.text}
            onClick={props.onActionClick}
          ></Button>
        </ButtonInnerContainer>

        {props.notExit ? (
          ''
        ) : (
          <ButtonInnerContainer>
            <Button
              width="100%"
              value="Avbryt"
              onClick={props.closeModal}
              style={{
                border: `2px solid ${theme.colors.secondary}`,
                background: 'white',
                color: theme.colors.secondary,
              }}
            ></Button>
          </ButtonInnerContainer>
        )}
      </ButtonOuterContainer>
    </Modal>
  );
};

const Cross = styled.img`
  width: 15px;
  height: 15px;
`;

export default ConfirmationModal;
