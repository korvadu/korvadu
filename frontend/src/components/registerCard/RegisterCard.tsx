import React from 'react';

import styled from 'styled-components';
import Dropdown from '../dropdown/Dropdown';

interface Props {
  id: number;
  campus: string;
  building: string;
  room: string;
  start: string;
  end: string;
  onDelete: (id: number) => void;
  onEdit: (id: number) => void;
  onReuse: (id: number) => void;
}

const RegisterCardContainer = styled.div`
  background: white;
  box-shadow: 1px 1px 2px #888888;
  margin: 10px;
  padding: 20px;
  border-radius: 10px;

  display: flex;
  align-items: center;
`;
const LeftColumn = styled.div`
  flex: 1;
`;
const RigthColumn = styled.div`
  height: 100%;
  flex-basis: 60px;
`;
const RegisterCardText = styled.div``;

const RegisterCard = (props: Props) => {
  const changeOnClick = () => {
    props.onEdit(props.id);
  };

  const handleDelete = () => {
    props.onDelete(props.id);
  };

  const handleReuse = () => {
    props.onReuse(props.id);
  };

  return (
    <RegisterCardContainer>
      <LeftColumn>
        <RegisterCardText>{props.campus}</RegisterCardText>
        <RegisterCardText>{props.building}</RegisterCardText>
        <RegisterCardText>{props.room}</RegisterCardText>
        <RegisterCardText>
          {props.start} - {props.end}
        </RegisterCardText>
      </LeftColumn>
      <RigthColumn>
        <Dropdown
          onDelete={handleDelete}
          onEdit={changeOnClick}
          onReuse={handleReuse}
        ></Dropdown>
      </RigthColumn>
    </RegisterCardContainer>
  );
};

export default RegisterCard;
