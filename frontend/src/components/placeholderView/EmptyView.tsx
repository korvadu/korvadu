import React from 'react';
import styled from 'styled-components';
import Text from '../text/Text';
interface Props {
  image: string;
  text: string;
}

const EmptyView = ({ image, text }: Props) => {
  return (
    <EmptyViewContainer>
      <Icon src={image}></Icon>
      <Text>{text}</Text>
    </EmptyViewContainer>
  );
};

const Icon = styled.img`
  width: 70px;
  margin-bottom: 40px;
`;
const EmptyViewContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

export default EmptyView;
