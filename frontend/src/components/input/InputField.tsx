import React from 'react'
import styled from 'styled-components'
import theme from '../../shared/theme/theme'

interface InputFieldProps {
    type: string;
    value: string;
    onChange: any;
    label?: string;
};

const InputFieldContainer = styled.div`
    position: relative;
    display: flex;
    justify-content: center;
`;

const InputFieldInput = styled.input`
    text-align: center;
    color: ${theme.colors.black};
    font-size: 20px;
    text-transform: capitalize;
    border: 2px solid ${theme.colors.primary};
    border-radius: 0.3em;
    padding: 10px;
    max-width: 260px;
    width: 80vw;

`;

const InputFieldParent = styled.div`
    display: inline-block;
    margin: 2rem auto;
    margin-bottom: 1rem;
`

const InputFieldLabel = styled.label`
    display: block;
`

const InputField = ({type, value, onChange, label}: InputFieldProps) => {
    return (
        <InputFieldContainer>
            <InputFieldParent>
                {label && <InputFieldLabel>{label}</InputFieldLabel>}
                <InputFieldInput type={type} defaultValue={value} onChange={onChange}/>
            </InputFieldParent>
        </InputFieldContainer>
    )
};

export default InputField;