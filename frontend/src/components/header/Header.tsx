import React from 'react';
import styled from 'styled-components';
import theme from '../../shared/theme/theme';
interface HeaderProps {
  text: string;
  imageSource: string;
}

const HeaderContainer = styled.div`
  position: relative;
  background: ${theme.colors.background};
  margin-bottom: 11vh;
`;

const HeaderText = styled.p`
  text-align: center;
  color: white;
  font-size: 20px;
  font-weight: bold;
  text-transform: uppercase;
  z-index: 1;
  position: relative;
  @media (max-width: 800px) {
    font-size: 17px;
  }
`;

const HeaderImageContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const HeaderImage = styled.img`
  margin-top: 2vh;
  height: 15vh;
  margin-bottom: 10px;
  z-index: 1;
  @media (max-width: 800px) {
    height: 13vh;
  }
  max-height: 250px;
`;

const Wave = styled.img`
  position: absolute;

  height: 60vh;
  top: 0vh;

  width: 120vw;
  right: -15vw;
  pointer-events: none;
  @media (max-width: 800px) {
    width: 300vw;
    left: 0vw;
    height: 47vh;
    top: 0vh;
  }
`;

const Header = ({ text, imageSource }: HeaderProps) => {
  return (
    <HeaderContainer>
      <HeaderImageContainer>
        <HeaderImage src={imageSource} />
      </HeaderImageContainer>
      <HeaderText>{text}</HeaderText>
      <Wave src="/background-vectors/header.svg"></Wave>
    </HeaderContainer>
  );
};

export default Header;
