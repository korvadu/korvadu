import React, { useState } from 'react';
import styled, { css } from 'styled-components';
import theme from '../../shared/theme/theme';
import HamburgerIcon from './HamburgerIcon';
import MenuLinks from '../../navigation/MenuLinks';

interface Props {}
interface HamburgerStatus {
  open: boolean;
}

// Based on https://codepen.io/erikterwan/pen/EVzeRP
const Hamburger = (props: Props) => {
  const [open, setOpen] = useState(false);

  const handleOnClick = () => {
    setOpen(!open);
  };

  return (
    <>
      <MenuContainer>
        <Input type="checkbox" open={open} onClick={handleOnClick} />
        <HamburgerIcon />
        <Menu>
          <Header open={open}>Korvadu?</Header>
          <MenuLinks handleOnClick={handleOnClick}></MenuLinks>
          <Logo src="/logos/logo-ntnu.png"></Logo>
        </Menu>
      </MenuContainer>
      <InvisibleContainer open={open} onClick={handleOnClick} />
    </>
  );
};

const Header = styled.p<HamburgerStatus>`
  color: ${theme.colors.primary};
  font-size: 25px;
  position: absolute;
  top: 65px;
  left: 100px;
  font-weight: bold;
  opacity: 0;
  transition: opacity 1.2s;
  ${(props) =>
    props.open &&
    css`
      opacity: 1;
    `};
`;

const InvisibleContainer = styled.div<HamburgerStatus>`
  position: absolute;
  top: 0px;
  bottom: 0px;
  width: 100%;
  height: 100%;
  background-color: transparent;
  display: none;
  ${(props) =>
    props.open &&
    css`
      display: block;
    `};
`;

const Input = styled.input<HamburgerStatus>`
  display: block;
  width: 40px;
  height: 32px;
  position: absolute;
  top: -7px;
  left: -5px;
  cursor: pointer;
  opacity: 0;
  z-index: 2;
  -webkit-touch-callout: none;
  ${(props) =>
    props.open &&
    css`
      & ~ span {
        opacity: 1;
        transform: rotate(45deg) translate(0px, -2px);
        background: #232323;
      }
      & ~ span:nth-last-child(3) {
        opacity: 0;
        transform: rotate(0deg) scale(0.2, 0.2);
      }
      & ~ span:nth-last-child(2) {
        transform: rotate(-45deg) translate(0, -2px);
      }
      & ~ ul {
        transform: none;
      }
    `}
`;

const Menu = styled.ul`
  position: absolute;
  width: 275px;
  margin: -100px 0 0 -50px;
  padding: 50px;
  padding-top: 125px;
  height: calc(100vh - 137px);
  background: ${theme.colors.white};
  list-style-type: none;
  -webkit-font-smoothing: antialiased;
  transform-origin: 0% 0%;
  transform: translate(-100%, 0);
  transition: transform 0.5s cubic-bezier(0.77, 0.2, 0.05, 1);
`;

const Logo = styled.img`
  position: absolute;
  width: 100px;
  bottom: 20px;
`;

const MenuContainer = styled.div`
  display: block;
  position: absolute;
  top: 35px;
  left: 35px;
  z-index: 99;
  -webkit-user-select: none;
  user-select: none;
`;

export default Hamburger;
