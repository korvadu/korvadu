import React from 'react';
import styled from 'styled-components';
import theme from '../../shared/theme/theme';

interface Props {}

const HamburgerIcon = (props: Props) => {
  return (
    <>
      <HamburgerLine></HamburgerLine>
      <HamburgerLine></HamburgerLine>
      <HamburgerLine></HamburgerLine>
    </>
  );
};

const HamburgerLine = styled.span`
  display: block;
  width: 33px;
  height: 4px;
  margin-bottom: 5px;
  position: relative;
  background: ${theme.colors.white};
  border-radius: 3px;
  z-index: 1;
  transform-origin: 4px 0px;
  transition: transform 0.5s cubic-bezier(0.77, 0.2, 0.05, 1),
    background 0.5s cubic-bezier(0.77, 0.2, 0.05, 1), opacity 0.55s ease;
  &:first-child {
    transform-origin: 0% 0%;
  }
  &:span:nth-last-child(2) {
    transform-origin: 0% 100%;
  }
`;

export default HamburgerIcon;
