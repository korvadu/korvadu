import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter } from 'react-router-dom';
import Routes from './navigation/Routes';
import registrationDuck from '../src/redux/registrationDuck';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { loadUser as loadUserOIDC, reducer as oidcReducer } from 'redux-oidc';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import promise from 'redux-promise-middleware';
import { logger } from 'redux-logger';
import mazemapDuck from './redux/mazemapDuck';
import userDuck from './redux/userDuck';
import { composeWithDevTools } from 'redux-devtools-extension';
import userManager from './auth/userManager';
import AuthenticationWrapper from './auth/authenticationWrapper';
import { registerLocale, setDefaultLocale } from 'react-datepicker';
import nb from 'date-fns/locale/nb';
import 'react-datepicker/dist/react-datepicker.css';
import { ToastContainer } from 'react-toastify';
import { OidcProvider } from 'redux-oidc';
import * as Sentry from '@sentry/react';
import {
  axiosInterceptor,
  axiosResponseInterceptor,
} from './shared/functions/functions';
import 'react-toastify/dist/ReactToastify.css';
import 'fix-date';
const ignoreErrors = ['Search request cancelled due to new search initiated'];

Sentry.init({
  ignoreErrors: ignoreErrors,
  dsn:
    'https://bc9926e2c0ed45c9870e029a92d4ca17@o434165.ingest.sentry.io/5390734',
});

registerLocale('nb', nb);
setDefaultLocale(nb);

const rootReducer = combineReducers({
  registration: registrationDuck,
  mazemap: mazemapDuck,
  oidc: oidcReducer,
  user: userDuck,
});
const composeEnhancers = composeWithDevTools({});
const sentryReduxEnhancer = Sentry.createReduxEnhancer({});

const enhancers = composeEnhancers(
  applyMiddleware(promise, logger, thunk),
  sentryReduxEnhancer
);

const store = createStore(rootReducer, enhancers);
loadUserOIDC(store, userManager);

axiosInterceptor(store);
axiosResponseInterceptor(store);

ReactDOM.render(
  <Provider store={store}>
    <OidcProvider store={store} userManager={userManager}>
      <React.StrictMode>
        <BrowserRouter>
          <AuthenticationWrapper>
            <Routes />
            <ToastContainer
              className="toast-container"
              toastClassName="toastClass"
              closeButton={false}
              hideProgressBar={true}
              position="bottom-right"
            />
          </AuthenticationWrapper>
        </BrowserRouter>
      </React.StrictMode>
    </OidcProvider>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
