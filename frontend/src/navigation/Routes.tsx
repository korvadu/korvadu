import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Registration from '../views/registration/Registration';
import Home from '../views/home/Home';
import MyRegistrations from '../views/my-registrations/MyRegistrations';
import MyProfile from '../views/my-profile/MyProfile';
import Callback from '../auth/Callback';
import Login from '../views/login/Login';
import { useSelector } from 'react-redux';
import Infected from '../views/infected/Infected';

interface RoutesProps {}

const Routes: React.FC<RoutesProps> = () => {
  const oidc = useSelector((state: any) => state.oidc);
  const authenticatedRoutes = () => (
    <Switch>
      <Route path="/infected">
        <Infected />
      </Route>
      <Route path="/information">
        <Home />
      </Route>
      <Route path="/my-registrations">
        <MyRegistrations />
      </Route>
      <Route path="/my-profile">
        <MyProfile />
      </Route>
      <Route path="/edit">
        <Registration pageType="edit" />
      </Route>
      <Route path="/reuse">
        <Registration pageType="reuse" />
      </Route>
      <Route path="/">
        <Registration pageType="normal" />
      </Route>
    </Switch>
  );
  const notAuthenticatedRoutes = () => (
    <Switch>
      <Route path="/callback">
        <Callback />
      </Route>
      <Route path="/">
        <Login />
      </Route>
    </Switch>
  );
  if (oidc.user && !oidc.user.expired) {
    return authenticatedRoutes();
  }
  return notAuthenticatedRoutes();
};

export default Routes;
