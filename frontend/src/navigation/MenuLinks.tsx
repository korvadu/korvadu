import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import theme from '../shared/theme/theme';
import { useDispatch } from 'react-redux';
import { logout } from '../shared/functions/functions';

interface Props {
  handleOnClick: () => void;
}

const MenuLinks = ({ handleOnClick }: Props) => {
  const dispatch = useDispatch();
  const handleLogout = () => {
    logout(dispatch);
  };
  return (
    <>
      <MenuLink>
        <MenuIcon src="/svg-icons/registration.svg"></MenuIcon>
        <Link to="/" onClick={handleOnClick}>
          Registrer opphold
        </Link>
      </MenuLink>
      <MenuLink>
        <MenuIcon src="/svg-icons/registration-list.svg"></MenuIcon>
        <Link to="/my-registrations" onClick={handleOnClick}>
          Mine registreringer
        </Link>
      </MenuLink>
      <MenuLink>
        <MenuIcon src="/svg-icons/covid.svg"></MenuIcon>
        <Link to="/infected" onClick={handleOnClick}>
          Dersom du er smittet
        </Link>
      </MenuLink>
      <MenuLink>
        <MenuIcon src="/svg-icons/about.svg"></MenuIcon>
        <Link to="/information" onClick={handleOnClick}>
          Om Korvadu?
        </Link>
      </MenuLink>
      <MenuLink>
        <MenuIcon src="/svg-icons/profile.svg"></MenuIcon>
        <Link to="/my-profile" onClick={handleOnClick}>
          Min profil
        </Link>
      </MenuLink>
      <MenuLink>
        <MenuIcon src="/svg-icons/logout.svg"></MenuIcon>
        <Link to="/" onClick={handleLogout}>
          Logg ut
        </Link>
      </MenuLink>
    </>
  );
};

const MenuIcon = styled.img`
  width: 25px;
  height: 25px;
  margin-right: 15px;
  margin-top: 4px;
`;

const MenuLink = styled.li`
  display: flex;
  padding: 10px 0;
  font-size: 22px;
  a {
    text-decoration: none;
    color: #232323;

    transition: color 0.3s ease;
  }
  a:hover {
    color: ${theme.colors.primary};
  }
`;

export default MenuLinks;
