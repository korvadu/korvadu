import userManager from '../../auth/userManager';
import { clearUser } from '../../redux/userDuck';
import { Dispatch, Store } from 'redux';
import infoToast from '../../components/toast/infoToast';
import axios, { AxiosError } from 'axios';
import { formatISO } from 'date-fns';
import errorToast from '../../components/toast/errorToast';

export const logout = (dispatch: Dispatch<any>) => {
  userManager.signoutPopup();
  dispatch(clearUser());
};

export const handleHttpResponse = (response: any, successMessage: string) => {
  response.then((data) => {
    const statusCode = data?.value?.status;
    if (statusCode >= 200 && statusCode <= 300) {
      infoToast(successMessage);
    }
  });
};

export const axiosInterceptor = (store: Store) => {
  return axios.interceptors.request.use((config) => {
    const setAuth = () => {
      const idToken = store.getState().oidc.user?.id_token;
      const accessToken = store.getState().oidc.user?.access_token;

      config.headers.Authorization = 'bearer ' + idToken;
      config.headers.AccessToken = 'bearer ' + accessToken;
    };

    const formatDate = () => {
      let data = config.data ? config.data : {};
      if (data.starttime) {
        config.data.starttime = formatISO(data.starttime);
      }
      if (data.endtime) {
        config.data.endtime = formatISO(data.endtime);
      }
    };

    setAuth();
    formatDate();

    return config;
  });
};

export const axiosResponseInterceptor = (store: Store) => {
  return axios.interceptors.response.use(
    (response) => {
      return response;
    },
    (error: AxiosError) => {
      if (error?.response?.status === 401) {
        logout(store.dispatch);
      } else if (error?.response?.status >= 400) {
        errorToast(error.response.data);
      } else {
        errorToast(error.message);
      }
      return Promise.reject(error);
    }
  );
};
