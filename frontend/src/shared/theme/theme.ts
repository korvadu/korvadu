const theme = {
  colors: {
    primary: '#00509e',
    secondary: '#6982FF',
    white: 'white',
    black: 'black',
    borderGray: 'hsl(0, 0%, 80%)',
    lightGray: '#f8f8f8',
    background: '#f6f7fa',
    gray: '#4D4855',
    disabled: '#d3d3d3',
  },
  font: {
    fontFamily: "'Open Sans', sans-serif",
  },
};
export default theme;
