// The ids are from Mazemap
export const campuses = [
  {
    campusId: 1,
    name: 'NTNU - Gløshaugen',
  },
  {
    campusId: 3,
    name: 'St. Olavs hospital',
  },
  {
    campusId: 294,
    name: 'NTNU - Sluppen',
  },
  {
    campusId: 148,
    name: 'NTNU - HUNT',
  },
  {
    campusId: 91,
    name: 'NTNU - Ålesund',
  },
  {
    campusId: 140,
    name: 'NTNU, UiB & Innovation Norway - Tokyo',
  },
  {
    campusId: 141,
    name: 'NTNU, UiB & SINTEF - Brussel',
  },
  {
    campusId: 18,
    name: 'NTNU - Dragvoll',
  },
  {
    campusId: 370,
    name: 'NTNU - Oslo',
  },
  {
    campusId: 20,
    name: 'NTNU - Sydområdet',
  },
  {
    campusId: 21,
    name: 'NTNU - Kalvskinnet',
  },
  {
    campusId: 22,
    name: 'NTNU - Tyholt',
  },
  {
    campusId: 54,
    name: 'NTNU - Tunga',
  },
  {
    campusId: 24,
    name: 'NTNU - Brattørkaia',
  },
  {
    campusId: 25,
    name: 'NTNU - Heggdalen',
  },
  {
    campusId: 26,
    name: 'NTNU - Ringve',
  },
  {
    campusId: 27,
    name: 'NTNU - Solsiden',
  },
  {
    campusId: 28,
    name: 'NTNU - Olavskvartalet',
  },
  {
    campusId: 29,
    name: 'NTNU - Moholt',
  },
  {
    campusId: 55,
    name: 'NTNU - Gjøvik',
  },
];
