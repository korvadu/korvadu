import { UserState } from 'redux-oidc';

export interface ReduxAction {
  type: string;
  payload: any;
}

export interface Registration {
  id?: number;
  userid?: number;
  room: string;
  building: string;
  campus: string;
  poiId: number;
  starttime: Date;
  endtime: Date;
}

export interface SelectedOptionNumber {
  value: number;
  label: string;
}

export interface SelectedRoom {
  value: RoomInfo;
  label: string;
}

export interface RoomInfo {
  room: string;
  building: string;
  poiId: number;
  campus: string;
}

export type PageType = 'normal' | 'edit' | 'reuse';

export interface UserInformation {
  email: string;
  fullname: string;
  id?: number;
  phonenumber?: string;
}

export interface RootState {
  registration: RegistrationState;
  user: User;
  oidc: UserState;
  mazemap: any;
}

export interface RegistrationState {
  error?: string;
  loading: boolean;
  registrations: Registration[];
}

export interface User {
  loading: boolean;
  user: UserInformation;
}
