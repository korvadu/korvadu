import { useSelector, useDispatch } from 'react-redux';
import React, { useEffect } from 'react';
import { loadUser } from '../redux/userDuck';

const AuthenticationWrapper = ({ children }) => {
  const oidc = useSelector((state: any) => state.oidc);
  const dispatch = useDispatch();
  useEffect(() => {
    if (oidc.user) {
      dispatch(loadUser(oidc));
    }
  }, [oidc, dispatch]);

  return <>{children}</>;
};

export default AuthenticationWrapper;
