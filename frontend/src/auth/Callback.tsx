import React from 'react';
import { CallbackComponent } from 'redux-oidc';
import userManager from './userManager';
import { useHistory } from 'react-router-dom';

interface IDispatchProps {}

type Props = IDispatchProps;

const Callback: React.FunctionComponent<Props> = (props) => {
  const history = useHistory();
  const doSuccessCallback = () => {
    history.push('/');
  };
  const doErrorCallback = () => {
    history.push('/login');
  };
  return (
    <CallbackComponent
      userManager={userManager}
      successCallback={doSuccessCallback}
      errorCallback={doErrorCallback}
    >
      <div>Redirecting...</div>
    </CallbackComponent>
  );
};

export default Callback;
