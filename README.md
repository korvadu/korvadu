[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.idi.ntnu.no/#https://gitlab.stud.idi.ntnu.no/korvadu/korvadu) 

# korvadu

"Kor va du?" er en minimalistisk sporingsapp for NTNU sine studenter og ansatte (på sikt kanskje også for gjester?).
Den baserer seg på egenregistrering av hvor en har vært og når, slik det ved smitteutbrudd kan kan hentes ut relevante data om potensielt smittede.

Systemet består av en app (korvadu-appen) for egenregistrering (og administrasjon av tidligere registreringer), og
en database-applikasjon (korvadu-tjeneren) som app-en kobler seg mot.

Av personvernhensyn så begrenses dette i første omgang, så det kun er den enkelte som basert på Feide-innlogging, kan se og redigere egenregistreringer.
Det er altså ikke planlagt noe administrasjonsgrensesnitt siden dataene er såpass enkle at det vil være hensiktsmessig med direkte uttrekk fra databasen ved en smittesituasjon.
På sikt kan en tenke seg ulike roller, som beredskapsansvarlig, emne-/arrangementsansvarlig, med funksjon for å få oversikt over relevante registreringer.

# Om repoet

Repoet er tenkt brukt til utbrodering av krav og utprøving av løsninger. Det er gitpodifisert med et docker-image for diverse språk og byggesystemer og med MySQL.

# Kontekst for bruk

Korvadu-appen er ment å bli brukt til å registrere hvor en har oppholdt seg i ulike tidsintervall,
hvor smitte kan overføres. For studenter vil dette typisk være undervisningsrom (auditorier, lab-er og grupperom). mens
det for ansatte kan være kontorer, møterom og undervisningsrom.

Akkurat hvilke opphold som skal registreres er et smittevernspørsmål som appen ikke legger seg opp i, men de vil typisk være
av en viss varighet og med en viss type kontakt med folk, luft og overflater slik at smitte er mulig.

Dette legges opp til at en både kan registrere at en ankommer en lokasjon og senere at en forlater det, eller registrere hele oppholdet i etterkant.
Begrepet lokasjon i campus-sammenheng forstås gjerne som rom, tilsvarende det en kan angi i Mazemap, men det er også aktuelt å angi
plassering innen et rom, f.eks. plassnummer (rent teknisk kan rom angis med en egen poi/rom-id eller geo-lokasjon (lengde- og breddegrad) og nivå (etasje)).

De fleste undervisningsaktiviteter skjer i kontekst av timeplanfestede aktiviteter i et emne, så det bør være mulig å registrere en slik knytning.
Men også andre grunner til opphold på campus er relevant, f.eks. ad-hoc gruppearbeid, arrangement i regi av studentforeninger osv.
Men det er i seg selv ikke så relevant for smitte, annet enn for å spore evt. deltakere som ikke har registrert oppholdet, men som en tror var der.
Derfor bør knytning til info om arrangement fleksibel og løs, f.eks. som tags ala 'tdt4100', 'bedpres-kantega' som arrangementsansvarlig oppgir.

En grunn for kobling til arrangement kan være at det kan lette registrering, f.eks.
ved at app-en kan slå opp i kalenderen din og hente ut planlagt sted og tid for et opphold. Evt.
kobling til hvilke emner en er oppmeldt i kan være nyttig, men er ikke nødvendig (fra starten).

I noen sammenhenger vil en være litt rundt omkring, f.eks. kan en læringsassistent følge opp studenter i flere grupperom, så
det er snakk om flere lokasjoner pr. registrering, men de vil typisk være i nærheten av hverandre (f.eks. innen samme kartutsnitt).

Tidsintervall bør være "passe", f.eks. innenfor en halvtime. Det er urimelig å forvente
veldig presise tidsstempel, ved et smitteutbrudd må en uansett utvide søket noe, rent tidsmessig.

Det er avgjørende at registrering er lettvint, så app-en må kunne fungere på alt av mobiler, nettbrett, bærbare eller stasjonære.
Opphold som er det aktuelt å registere er innendørs, så en er rimelig beskyttet, men designmessig må
en ta høyde for at registrering kanskje skjer på farten fra et sted til et annet, f.eks. på bussen.

Feide-innlogging bør fungere for de fleste brukere, men det kan tenkes at gjester uten slik mulighet må kunne bruke app-en.
Derfor kan autentisering med OAuth, som støttes av Google, Facebook og andre vurderes på sikt.

# Scenarier

Her presenteres ulike scenarier for bruk, for å illustrere hvilke krav som stilles.

## Scenario 1 - Forelesning i et auditorium eller større sal

En ankommer auditoriet, åpner appen og angir at en har begynt på et opphold.
Stedet angis basert på innendørs posisjoner eller ved å finne rommet med Mazemap.
Forslag basert på tidligere opphold på samme tid (i uka og på dag) kan også være til hjelp.
Dersom plassen har et nummer, så angis det. Registrering tagges med emnekode, som
kan foreslås basert på tidligere registreringer.

## Scenario 2 - Arbeid med øvinger på lab.

Student har fått avsatt arbeidstid på lab/sal for arbeid med øving i et emne. Læringsassistent er tilgjengelig på eget rom.
Student registrerer at aktivitet starter, når hen har funnet en plass.
Underveis oppsøkes læringsassistenten (på et rom i nærheten), så det registreres rett i etterkant.
Når arbeidstiden er omme, registreres at oppholdet er avsluttet.

## Scenario 3 - Læringsassistent har veiledning

Læringsassistent har jobb med å følge opp studenter i et emne, som er spredt på to-tre rom.
Stort sett sitter hen i ro og oppsøkes av studentene (som oppfordres til å registrere det), men
hen går innimellom innom salene og hører med dem om hvordan det går.

Oppholdet registreres i etterkant i kantina, både egen plass og studentsalene knyttes til hele intervallet.

## Illustrerende skisser

<img src="sketch.png" width="400">
