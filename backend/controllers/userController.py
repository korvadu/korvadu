from flask import Blueprint, Response, jsonify, request, abort, make_response
import json
from services import userService
from auth import requiresUser, requiresIdToken, get_token_auth_header
from flask import _request_ctx_stack
from utils import dataporten
from urllib.error import HTTPError
from services import registrationService

user = Blueprint("user", __name__, url_prefix="/user")


def registerUser():
    ctx = _request_ctx_stack.top
    try:
        accessToken = get_token_auth_header("AccessToken")
    except (HTTPError, TypeError):
        return Response("{'error':'Access token not valid'}", 401)
    try:
        userInfo = dataporten.getDataportenUserInfo(accessToken)
        print(userInfo)
    except HTTPError:
        return Response("{'error':' Could not fetch dataporten info'}", 401)
    if ctx.idToken.get("sub") == userInfo.get("sub"):
        response, statusCode = userService.registerUser(userInfo)
        return make_response(jsonify(response), statusCode)
    else:
        return Response("{'error':' Could not fetch dataporten info'}", 401)


@user.route("/")
@requiresIdToken()
def getUserOrRegister():
    ctx = _request_ctx_stack.top
    sub = ctx.idToken.get("sub")
    user = userService.getUserFromSub(sub)
    if not user:
        return registerUser()
    return jsonify(user.to_json())


@user.route("/", methods=["DELETE"])
@requiresUser
def deleteSelf():
    ctx = _request_ctx_stack.top
    user = ctx.user
    responseDelete, deleteCode = registrationService.deleteRegistrationsByUserId(
        user.id
    )
    response, successCode = userService.deleteUser(user.id)

    if deleteCode == 200:
        return make_response(jsonify(response), successCode)
    return make_response(jsonify(response), deleteCode)


@user.route("/", methods=["PUT"])
@requiresUser
def updateSelf():
    ctx = _request_ctx_stack.top
    user = ctx.user

    if request.is_json:
        form = request.get_json()
        responseText, statusCode = userService.updatePhonenumber(user.id, form)
        return make_response(jsonify(responseText), statusCode)
    return abort(400)
