from flask import Blueprint, Response, jsonify, request, abort, make_response
import json
from services import registrationService
from auth import requiresUser, requiresIdToken, get_token_auth_header
from flask import _request_ctx_stack
from utils import dataporten
from urllib.error import HTTPError

registration = Blueprint("registration", __name__, url_prefix="/registration")

"""
@todo add @requiresUser
"""


@registration.route("/", methods=["POST"])
@requiresUser
def createRegistration():
    ctx = _request_ctx_stack.top
    user = ctx.user
    if request.is_json:
        form = request.get_json()
        responseText, statusCode = registrationService.createRegistration(
            form, userid=user.id
        )
        return make_response(jsonify(responseText), statusCode)
    return abort(400)


@registration.route("/<id>")
@requiresUser
def getRegistration(id):
    ctx = _request_ctx_stack.top
    user = ctx.user

    registration, statusCode = registrationService.getRegistrationById(
        id, userid=user.id
    )

    return (
        make_response(jsonify(registration.to_json()), statusCode)
        if statusCode == 200
        else make_response(jsonify(registration), statusCode)
    )


@registration.route("/")
@requiresUser
def getRegistrationsByUser():
    ctx = _request_ctx_stack.top
    user = ctx.user
    registrations = registrationService.getRegistrationByUserId(user.id)
    registrationList = list(map(lambda x: x.to_json(), registrations))
    return jsonify(registrationList) if registrations else Response("[]", 200)


@registration.route("/<id>", methods=["DELETE"])
@requiresUser
def deleteRegistration(id):
    ctx = _request_ctx_stack.top
    user = ctx.user
    reponseText, statusCode = registrationService.deleteRegistration(id, userid=user.id)

    return make_response(jsonify(reponseText), statusCode)


@registration.route("/<id>", methods=["PUT"])
@requiresUser
def updateRegistration(id):
    ctx = _request_ctx_stack.top
    user = ctx.user

    if request.is_json:
        form = request.get_json()
        responseText, statusCode = registrationService.updateRegistration(
            id, form, userid=user.id
        )

        return make_response(jsonify(responseText), statusCode)
    return abort(400)
