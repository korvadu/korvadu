import os
from flask import Flask
from shared import db
from flask_migrate import Migrate
from flask_cors import CORS
from controllers.userController import user
from controllers.registrationController import registration
from sqlalchemy_utils import database_exists, create_database

# You can change these values in the .env-filen
USER = os.getenv("MYSQL_USER")
PASSWORD = os.getenv("MYSQL_PASSWORD")
DB = os.getenv("MYSQL_DATABASE")
DEBUG = os.getenv("DEBUG")
HOST = os.getenv("MYSQL_HOST")
# The f is for string insertion
database_file = f"mysql+pymysql://{USER}:{PASSWORD}@mysql:3306/{DB}"

migrate = Migrate()


def create_app(config_filename=None, db_url=database_file):
    app = Flask(__name__)
    if config_filename:
        app.config.from_pyfile(config_filename)
    init_extensions(app, db_url)
    register_blueprints(app)
    return app


def init_extensions(app, db_url):
    app.config["SQLALCHEMY_DATABASE_URI"] = db_url
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True

    try:
        with app.app_context():
            if not database_exists(db.engine.url):
                create_database(db.engine.url)
    except:
        pass
    db.init_app(app)
    migrate.init_app(app, db)
    CORS(app, resources={r"*": {"origins": ["http://localhost:3000", "https://korvadu.it.ntnu.no", "https://korvadu.firebaseapp.com"]}})


def register_blueprints(app):
    app.register_blueprint(user)
    app.register_blueprint(registration)


app = create_app()


@app.after_request
def after(response):
    # todo with response
    print(response.status)
    print(response.headers)
    print(response.get_data())
    return response


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=bool(DEBUG))
