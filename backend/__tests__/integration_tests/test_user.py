from unittest.mock import patch, Mock
from __tests__.conftest import MysqlFactory
from main import create_app
from shared import db
from models.user import User
from flask import jsonify
from unittest import TestCase
from jose import jwt
import json
from __tests__.testUtils.constants import (
    accessToken,
    token,
    decodedToken,
    testGroups,
    dataportenResponse,
)
from __tests__.testUtils.authentication import mock_authentication_context
from __tests__.testUtils.models import createUser


def mock_jwt_decode():
    mock_decode = Mock(name="decode")
    mock_decode.return_value = decodedToken
    return mock_decode


"""
Integration-tests for authentication and usermanagement
"""


class TestUser(TestCase):
    def setUp(self):
        self.mysql = MysqlFactory()
        self.app = create_app(db_url=self.mysql.url())
        self.app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
        db.init_app(self.app)
        self.ctx = self.app.app_context()
        self.ctx.push()
        self.token = f"Bearer {token}"
        self.accessToken = f"Bearer {accessToken}"

    """
    Tests that a valid id_token returns the user accociated with it.
    """

    @mock_authentication_context
    def test_get_self_success(self):
        user = createUser(db.session)
        response = self.app.test_client().get(
            "http://localhost:5000/user/",
            headers={"Authorization": self.token, "AccessToken": self.accessToken},
        )
        dbUser = user.to_json()
        assert response.data == jsonify(dbUser).data

    """
    Test that you get 401 unAuthorized if the sub from dataporten does not correspond with your sub
    """

    @mock_authentication_context
    def test_create_should_fail(self):
        mock_dataporten = Mock(name="mock_dataporten")
        mock_dataporten.return_value = dataportenResponse.copy()
        mock_dataporten.return_value["sub"] = "123"

        headers = {
            "Authorization": self.token,
            "Content-type": "application/json",
            "AccessToken": self.accessToken,
        }

        with patch("utils.dataporten.getDataportenUserInfo", mock_dataporten):
            response = self.app.test_client().get(
                "http://localhost:5000/user/", headers=headers
            )
            assert response.status == "401 UNAUTHORIZED"

    """
    Test that a user is registered when calling getSelf without a registered user
    """

    @mock_authentication_context
    def test_create_should_succeed(self):
        headers = {
            "AccessToken": self.accessToken,
            "Authorization": self.token,
            "Content-type": "application/json",
        }

        response = self.app.test_client().get(
            "http://localhost:5000/user/", headers=headers
        )

        user = db.session.query(User).first()
        dbUser = user.to_json()
        assert response.status == "201 CREATED"
        assert user.email == dataportenResponse.get("email")
        assert json.loads(response.data) == json.loads(jsonify(dbUser).data)

    """
    Test that the user is deleted when delete is called
    """

    @mock_authentication_context
    def test_delete_self(self):
        user = createUser()
        db.session.add(user)
        db.session.commit()
        headers = {
            "Authorization": self.token,
            "Content-type": "application/json",
            "AccessToken": self.accessToken,
        }

        response = self.app.test_client().delete(
            "http://localhost:5000/user/", headers=headers
        )
        user = db.session.query(User).all()
        assert response.status == "200 OK"
        assert user == []

    def tearDown(self):
        self.mysql.stop()
        self.ctx.pop()
