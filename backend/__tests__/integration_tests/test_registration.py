from unittest.mock import patch, Mock
from __tests__.conftest import MysqlFactory
from main import create_app
from shared import db
from models.user import User
from models.registration import Registration
from flask import jsonify
from unittest import TestCase
from jose import jwt
import json
from __tests__.testUtils.constants import (
    accessToken,
    token,
    decodedToken,
    testGroups,
    dataportenResponse,
)
from __tests__.testUtils.authentication import mock_authentication_context
from __tests__.testUtils.models import createUser, createRegistration
import json


def mock_jwt_decode():
    mock_decode = Mock(name="decode")
    mock_decode.return_value = decodedToken
    return mock_decode


"""
Integration-tests for authentication and usermanagement
"""


class TestRegistration(TestCase):
    def setUp(self):
        self.mysql = MysqlFactory()
        self.app = create_app(db_url=self.mysql.url())
        self.app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
        db.init_app(self.app)
        self.ctx = self.app.app_context()
        self.ctx.push()
        self.token = f"Bearer {token}"
        self.accessToken = f"Bearer {accessToken}"

    """
    Test that you can create a registration 
    """

    @mock_authentication_context
    def test_create_registration_success(self):
        user = createUser(db.session)
        registration = createRegistration(user)
        request = registration.to_json()
        request["userid"] = None
        response = self.app.test_client().post(
            "http://localhost:5000/registration/",
            data=json.dumps(request),
            headers={
                "Authorization": self.token,
                "AccessToken": self.accessToken,
                "Content-Type": "application/json",
            },
        )
        assert db.session.query(Registration).first().userid == registration.userid

    """
    Test that you get 401 unAuthorized if id_token is not supplied
    """

    @mock_authentication_context
    def test_create_should_fail(self):
        user = createUser(db.session)
        registration = createRegistration(user)
        request = registration.to_json()
        request["userid"] = None
        response = self.app.test_client().post(
            "http://localhost:5000/registration/",
            data=json.dumps(request),
            headers={"Content-Type": "application/json"},
        )
        assert response.status == "401 UNAUTHORIZED"

    """
    Test that registration own by user is retrieved 
    """

    @mock_authentication_context
    def test_get_registration_succeds(self):
        user = createUser(db.session)
        registration = createRegistration(user, db.session)
        headers = {
            "AccessToken": self.accessToken,
            "Authorization": self.token,
            "Content-type": "application/json",
        }

        response = self.app.test_client().get(
            f"http://localhost:5000/registration/{registration.id}", headers=headers
        )

        assert response.status == "200 OK"
        assert response.data == jsonify(registration.to_json()).data

    """
    Test that a registration owned by another user is not retrieved
    """

    @mock_authentication_context
    def test_get_registration_should_fail(self):
        user = createUser(db.session)
        user2 = User(fullname="august", sub="123", email="august@sd.no")
        db.session.add(user2)
        db.session.commit()
        registration = createRegistration(user2, db.session)
        headers = {
            "Authorization": self.token,
            "Content-type": "application/json",
            "AccessToken": self.accessToken,
        }

        response = self.app.test_client().get(
            f"http://localhost:5000/registration/{registration.id}", headers=headers
        )
        assert response.status == "403 FORBIDDEN"

    """
    Test that registration is deleted
    """

    @mock_authentication_context
    def test_delete_registration_success(self):
        user = createUser(db.session)
        registration = createRegistration(user, db.session)
        headers = {
            "AccessToken": self.accessToken,
            "Authorization": self.token,
            "Content-type": "application/json",
        }

        response = self.app.test_client().delete(
            f"http://localhost:5000/registration/{registration.id}", headers=headers
        )

        assert response.status == "200 OK"
        assert db.session.query(Registration).all() == []

    """
    Test that a registration owned by another user is not deleted
    """

    @mock_authentication_context
    def test_delete_registration_should_fail(self):
        user = createUser(db.session)
        user2 = User(fullname="august", sub="123", email="august@sd.no")
        db.session.add(user2)
        db.session.commit()
        registration = createRegistration(user2, db.session)
        headers = {
            "Authorization": self.token,
            "Content-type": "application/json",
            "AccessToken": self.accessToken,
        }

        response = self.app.test_client().delete(
            f"http://localhost:5000/registration/{registration.id}", headers=headers
        )
        assert response.status == "403 FORBIDDEN"

    """
    Check that registration is updated after request, and user id is not updated
    """

    @mock_authentication_context
    def test_update_registration_success(self):
        user = createUser(db.session)
        registration = createRegistration(user, db.session)
        request = registration.to_json()
        request["poiId"] = 84488
        request["userid"] = "123"
        response = self.app.test_client().put(
            f"http://localhost:5000/registration/{registration.id}",
            data=json.dumps(request),
            headers={
                "Authorization": self.token,
                "AccessToken": self.accessToken,
                "Content-Type": "application/json",
            },
        )
        assert response.status == "200 OK"
        assert db.session.query(Registration).first().userid == registration.userid
        assert db.session.query(Registration).first().poiId == request["poiId"]

    """
    Test that a registration owned by another user is not updated
    """

    @mock_authentication_context
    def test_update_registration_should_fail(self):
        user = createUser(db.session)
        user2 = User(fullname="august", sub="123", email="august@sd.no")
        db.session.add(user2)
        db.session.commit()
        registration = createRegistration(user2, db.session)
        request = registration.to_json()
        request["poiId"] = "dragaluf"
        headers = {
            "Authorization": self.token,
            "Content-type": "application/json",
            "AccessToken": self.accessToken,
        }

        response = self.app.test_client().put(
            f"http://localhost:5000/registration/{registration.id}",
            data=json.dumps(request),
            headers=headers,
        )
        assert response.status == "403 FORBIDDEN"
        assert db.session.query(Registration).first().poiId != request["poiId"]

    """
    Test that getRegistrationsByUser only returns your own registrations
    """

    @mock_authentication_context
    def test_get_registrations_by_user(self):
        user = createUser(db.session)
        registration = createRegistration(user, db.session)
        user2 = User(fullname="august", email="hallo@sa.no", sub="123")
        db.session.add(user2)
        db.session.commit()
        registration2 = createRegistration(user2, db.session)
        headers = {
            "Authorization": self.token,
            "Content-type": "application/json",
            "AccessToken": self.accessToken,
        }

        response = self.app.test_client().get(
            "http://localhost:5000/registration/", headers=headers
        )
        assert response.status == "200 OK"
        assert response.data == jsonify([registration.to_json()]).data

    def tearDown(self):
        self.mysql.stop()
        self.ctx.pop()
