from __tests__.testUtils.constants import (
    decodedToken,
    token,
    accessToken,
    testGroups,
    dataportenResponse,
)
from __tests__.testUtils.models import createUser
from models.user import User
from unittest.mock import Mock, patch
from functools import wraps


"""
Helper function for mocking authentication if mocker is used
"""


def mock_authentication(mocker):
    user = createUser()
    mock_decode = mocker.Mock(name="decode")
    mock_decode.return_value = decodedToken
    mock_dataporten_getUserInfo = mocker.Mock(name="mock_dataporten")
    mock_dataporten_getUserInfo.return_value = dataportenResponse
    mock_access_token = mocker.Mock(name="getAccessToken")
    mock_access_token.return_value = accessToken, True
    mocker.patch("utils.dataporten.getDataportenUserInfo", mock_dataporten_getUserInfo)
    mocker.patch("jose.jwt.decode", mock_decode)
    mocker.patch("jose.jwt.get_unverified_header", lambda x: decodedToken)
    mocker.patch("utils.dataporten.getDataportenGroups", lambda x: testGroups)
    mocker.patch("utils.dataporten.checkIfAdmin", lambda x: True)
    mocker.patch("auth.get_token_auth_header", lambda x: token)
    mocker.patch("auth.getAccessToken", mock_access_token)
    mocker.patch("services.userService.getUserFromSub", lambda x: user)


"""
Decorator for mocking authentication
"""


def mock_authentication_context(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        mock_decode = Mock(name="decode")
        mock_decode.return_value = decodedToken
        mock_dataporten_getUserInfo = Mock(name="mock_dataporten")
        mock_dataporten_getUserInfo.return_value = dataportenResponse
        with patch(
            "utils.dataporten.getDataportenUserInfo", mock_dataporten_getUserInfo
        ), patch("jose.jwt.decode", mock_decode), patch(
            "jose.jwt.get_unverified_header", lambda x: decodedToken
        ), patch(
            "utils.dataporten.getDataportenGroups", lambda x: testGroups
        ):
            return f(*args, **kwargs)

    return decorator
