from models.user import User
from models.registration import Registration
from datetime import datetime, timedelta

"""
Creates a user, should be used when testing instead of creating new users directly from model
"""


def createUser(db_session=None):
    user = User(
        sub="55de7d71-4a25-4103-8e43-35df8c2d472a",
        email="4s8j0rng@ELEV_GGGGGG.no",
        fullname="Asbjørn ELEVG baby",
    )
    if db_session:
        db_session.add(user)
        db_session.commit()
    return user


def createRegistration(user, db_session=None):
    registration = Registration(
        userid=user.id,
        room="R1",
        building="Realfagsbygget",
        campus="NTNU Gløshaugen",
        poiId=123,
        starttime="2020-08-15 08:38:57",
        endtime="2020-08-15 08:38:57")
    if db_session:
        db_session.add(registration)
        db_session.commit()
    return registration
