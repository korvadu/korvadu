from services import userService
from shared import db
from __tests__.testUtils.models import createUser


def setUp(mocker):
    mocker.patch.object(db.session, "add")
    mocker.patch.object(db.session, "commit")
    mocker.patch.object(db.session, "delete")


"""
Test that userService.registerUser creates a user, database is mocked. 
"""


def test_register_user(mocker):
    setUp(mocker)
    userInfo = {
        "sub": "123",
        "name": "Asbjørn ELEVG baby",
        "email": "4s8j0rng@ELEV_GGGGGG.no",
        "email_verified": True,
        "picture": "https://auth.dataporten.no/openid/userinfo/v1/user/media/p:a3019954-902f-45a3-b4ee-bca7b48ab507",
    }
    responseText, statusCode = userService.registerUser(userInfo)
    user = createUser().to_json()
    assert 201 == statusCode
    assert user == responseText
    db.session.add.assert_called()
    db.session.commit.assert_called()
