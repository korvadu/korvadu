from models.user import User
from __tests__.testUtils.models import createUser

"""
Test that a user is created in the database when commit is called
"""


def test_a_transaction(db_session):
    test_user = createUser()

    db_session.add(test_user)
    db_session.commit()

    assert test_user.sub == db_session.query(User).one().sub


"""
Test that database has no users from the start. 
Is a sanity-check for debugging of tests
"""


def test_db_drop(db_session):
    test_user = db_session.query(User).first()
    assert test_user is None


"""
def test_cascading(db_session):
    application = createApplication(db_session)
    assert not db_session.query(Application).first() is None
    assert not db_session.query(User).first() is None
    db_session.delete(application.user)
    db_session.commit()
    assert db_session.query(Application).first() is None
    assert db_session.query(User).first() is None
"""
