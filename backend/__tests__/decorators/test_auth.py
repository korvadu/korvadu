from unittest.mock import Mock
import auth
from jose import jwt
from flask import _request_ctx_stack
from main import app
from __tests__.testUtils.constants import token, accessToken, decodedToken, rsa_key
from __tests__.testUtils.models import createUser
from services import userService
from models.user import User
import os


"""
Test that jwt.decode is called with the correct parameters when you decorate a function.
Test that the id_token is saved in the request context when when you decorate a function with requiresIDToken
"""


def test_requires_id_token(mocker):
    mock_jwt(mocker)
    func = Mock()
    decorated_func = auth.requiresIdToken()(func)

    with app.test_request_context():
        decorated_func()
        assert _request_ctx_stack.top.idToken == decodedToken
        jwt.decode.assert_called_with(
            token,
            rsa_key,
            algorithms=["RS256"],
            issuer="https://auth.dataporten.no",
            audience=os.getenv("DATAPORTEN_CLIENT_ID"),
            options={
                "verify_signature": True,
                "verify_exp": True,
                "verify_at_hash": True,
            },
            access_token="eca448c7-b0f0-487d-98c5-946c3bb29868",
        )


"""
Test that the id_token is saved in the request context when when you decorate a function with requiresIDToken
Test that a user is in the request context when a function is decorated with requiresUser
"""


def test_requires_user(mocker):
    user = createUser()
    mock_jwt(mocker)

    mocker.patch.object(userService, "getUserFromSub")
    userService.getUserFromSub.return_value = user

    func = Mock()
    decorated_func = auth.requiresUser(func)
    with app.test_request_context():
        decorated_func()
        assert _request_ctx_stack.top.idToken == decodedToken
        assert _request_ctx_stack.top.user == user
        userService.getUserFromSub.assert_called_with(decodedToken.get("sub"))


def mock_jwt(mocker):
    mocker.patch.object(auth, "get_token_auth_header")
    auth.get_token_auth_header = (
        lambda x: token if x == "Authorization" else accessToken
    )
    mocker.patch.object(jwt, "decode")
    mocker.patch.object(jwt, "get_unverified_header")
    jwt.decode.return_value = decodedToken
    jwt.get_unverified_header.return_value = decodedToken
    auth.get_token_auth_header.return_value = token
