from models.user import User
from shared import db
import re
from sqlalchemy.exc import SQLAlchemyError


def getUserByUsername(username):
    user = db.session.query(User).filter(User.username == username).first()
    return user


def getUserById(id):
    user = db.session.query(User).get(id)
    return user


def registerUser(userInfo):
    user = User(
        sub=userInfo.get("sub"),
        email=userInfo.get("email"),
        fullname=userInfo.get("name"),
        phonenumber=userInfo.get("phonenumber"),
    )
    db.session.add(user)
    db.session.commit()
    return user.to_json(), 201


def getUserFromSub(sub):
    user = db.session.query(User).filter(User.sub == sub).first()
    return user


def getApplicationByStudentNumber(studentNumber):
    user = db.session.query(User).filter(User.studentNumber == studentNumber).first()
    return user


def deleteUser(id):
    try:
        user = getUserById(id)
        db.session.delete(user)
        db.session.commit()
        return user.to_json(), 200
    except SQLAlchemyError as err:
        print(err, flush=True)
        return "", 400
    except AssertionError as err:
        print(err, flush=True)
        return "", 400


def getAllUsers():
    users = db.session.query(User).all()
    return users


def updatePhonenumber(id, form):
    try:
        user = getUserById(id)
        setattr(user, "phonenumber", form["phonenumber"])
        db.session.add(user)
        db.session.commit()
        return user.to_json(), 200
    except SQLAlchemyError as err:
        print(err)
        return "", 400
