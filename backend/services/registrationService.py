from models.registration import Registration
from shared import db
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy import desc, asc
from dateutil.parser import parse


def createRegistration(info, userid):
    registration = Registration(
        userid=userid,
        room=info.get("room"),
        building=info.get("building"),
        campus=info.get("campus"),
        poiId=info.get("poiId"),
        starttime=parse(info.get("starttime")),
        endtime=parse(info.get("endtime")),
    )
    db.session.add(registration)
    db.session.commit()
    return registration.to_json(), 201


def getRegistrationById(id, userid):
    registration = db.session.query(Registration).get(id)

    if registration.userid != userid:
        return "{'error': 'You dont own this registration'}", 403

    return registration, 200


def getRegistrationByUserId(userid):
    registration = (
        db.session.query(Registration)
        .filter(Registration.userid == userid)
        .order_by(desc(Registration.starttime))
        .all()
    )
    return registration


def deleteRegistration(id, userid):
    try:
        registration, statusCode = getRegistrationById(id, userid)

        # Check if user owns this object
        if statusCode != 200:
            return "{'error': 'You dont own this registration'}", 403

        db.session.delete(registration)
        db.session.commit()
        return registration.to_json(), 200
    except SQLAlchemyError as err:
        print(err, flush=True)
        return "", 400
    except AssertionError as err:
        print(err, flush=True)
        return "", 400


def deleteRegistrationsByUserId(userid):
    try:
        db.session.query(Registration).filter(Registration.userid == userid).delete()
        return "", 200
    except SQLAlchemyError as err:
        print(err, flush=True)
        return "", 400
    except AssertionError as err:
        print(err, flush=True)
        return "", 400


def updateRegistration(id, form, userid):
    try:
        registration, statusCode = getRegistrationById(id, userid=userid)

        # Check if user owns this object
        if statusCode != 200:
            return "{'error': 'You dont own this registration'}", 403

        for field in form.keys():
            if field == "id" or field == "userid":
                continue
            if field == "endtime" or field == "starttime":
                form[field] = parse(form[field])

            setattr(registration, field, form[field])
        db.session.add(registration)
        db.session.commit()
        return registration.to_json(), 200
    except SQLAlchemyError as err:
        print(err, flush=True)
        return "", 400


def getAllRegistrations():
    registration = (
        db.session.query(Registration).order_by(desc(Registration.starttime)).all()
    )
    return registration
