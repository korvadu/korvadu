from shared import db
import json


class Registration(db.Model):
    __tablename__ = "registration"

    id = db.Column("registration_id", db.Integer, primary_key=True)

    userid = db.Column(
        db.ForeignKey("users.userid", ondelete="CASCADE"),
        nullable=False,  # change to false
    )

    room = db.Column("room", db.String(60), unique=False)

    building = db.Column("building", db.String(60), unique=False)

    campus = db.Column("campus", db.String(60), unique=False)

    poiId = db.Column("poiId", db.Integer, unique=False)

    starttime = db.Column("start_time", db.DateTime, nullable=False)

    endtime = db.Column("end_time", db.DateTime, nullable=False)

    def __init__(self, userid, room, building, campus, poiId, starttime, endtime):
        self.userid = userid
        self.room = room
        self.building = building
        self.campus = campus
        self.poiId = poiId
        self.starttime = starttime
        self.endtime = endtime

    def to_json(self):
        return {
            "id": self.id,
            "userid": self.userid,
            "room": self.room,
            "building": self.building,
            "campus": self.campus,
            "poiId": self.poiId,
            "starttime": str(self.starttime),
            "endtime": str(self.endtime),
        }

    def __str__(self):
        return json.dumps(self.to_json())
