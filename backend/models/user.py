from shared import db
import json


class User(db.Model):
    __tablename__ = "users"
    id = db.Column("userid", db.Integer, primary_key=True)

    sub = db.Column("sub", db.String(50), unique=True,)

    fullname = db.Column("fullname", db.String(60), unique=False)

    email = db.Column("email", db.String(30), nullable=True)

    phonenumber = db.Column("phonenumber", db.String(11), nullable=True)

    def __init__(self, sub, email, fullname, phonenumber=None):
        self.fullname = fullname
        self.sub = sub
        self.email = email
        self.phonenumber = phonenumber

    def to_json(self):
        return {
            "id": self.id,
            "fullname": self.fullname,
            "email": self.email,
            "phonenumber": self.phonenumber,
        }

    def __str__(self):
        return json.dumps(self.to_json())
