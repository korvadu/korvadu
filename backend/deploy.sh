#!/bin/bash

source env/bin/activate

pip install -r requirements.txt

flask db init
flask db upgrade
flask db migrate
flask db upgrade

uwsgi --ini korvadu.ini --logto mylog.log
